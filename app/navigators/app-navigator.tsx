/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/display-name */
/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import React, { useEffect } from "react"
import { Platform, useColorScheme, TouchableOpacity, View, Image } from "react-native"
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
  useNavigation,
} from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { navigationRef } from "./navigation-utilities"
import HomeScreen from "../screens/home"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs"
import CongeScreen from "../screens/conge"
import HistoriqueScreen from "../screens/historique"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faBell,
  faCalendar,
  faFileInvoice,
  faHistory,
  faHome,
  faUser,
  faPlus,
  faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons"
import LoginScreen from "../screens/login"
import { color } from "../theme"
import { translate } from "../i18n"
import AdminNotificationScreen from "../screens/admin-notification"
import AdminPendingScreen from "../screens/admin-pending"
import AdminUserScreen from "../screens/admin-user"
import RegisterScreen from "../screens/Register"
import AdminAddCongeScreen from "../screens/admin-add-conge"
import AdminConsultCongeScreen from "../screens/admin-consult-conge"
import ConsultUserScreen from "../screens/admin-consult-user"
import RegisterSignatureScreen from "../screens/register-signature"
import Toast from "react-native-toast-message"
import { useUserStore } from "../models/user-store/user-store"
import { observer } from "mobx-react-lite"
import TabBarIcon from "../components/tab-bar-icon"
import AdminEditCommonHolidayScreen from "../screens/admin-edit-common-holiday"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type NavigatorParamList = {
  home: undefined
  login: undefined
  admin: undefined
  register: undefined
  adminConge: undefined
  registerSignature: undefined
  editCommonHoliday: undefined
}

export type HomeTabsParamList = {
  home: undefined
  conge: undefined
  historique: undefined
  notification: undefined
}

export type AdminTabsParamList = {
  pending: undefined
  notification: undefined
  user: undefined
  commonHoliday: undefined
}

export type CongeTabsParamList = {
  addConge: undefined
  consultConge: undefined
}
export type UserTabsParamList = {
  addConge: undefined
  consultConge: undefined
}

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const Stack = createNativeStackNavigator<NavigatorParamList>()

const Tab = createBottomTabNavigator<HomeTabsParamList>()
const TabAdmin = createBottomTabNavigator<AdminTabsParamList>()
const TabConge = createMaterialTopTabNavigator()
const TabUser = createMaterialTopTabNavigator()

const UserTabNavigator = () => {
  return (
    <TabUser.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarIndicatorStyle: { backgroundColor: color.palette.blue_oec },
        tabBarStyle: {
          height: Platform.OS === "ios" ? 100 : 55,
          backgroundColor: color.palette.white,
        },
      }}
    >
      <TabUser.Screen
        name="pendingUser"
        component={AdminUserScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faPlus}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <TabUser.Screen
        name="consultUser"
        component={ConsultUserScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faUser}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
    </TabUser.Navigator>
  )
}

const CongeTabNavigator = () => {
  return (
    <TabConge.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarIndicatorStyle: { backgroundColor: color.palette.blue_oec },
        tabBarStyle: {
          height: Platform.OS === "ios" ? 100 : 55,
          backgroundColor: color.palette.white,
        },
      }}
    >
      <TabConge.Screen
        name="consultConge"
        component={AdminConsultCongeScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faCalendar}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <TabConge.Screen
        name="addConge"
        component={AdminAddCongeScreen}
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faPlus}
                size={30}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
    </TabConge.Navigator>
  )
}

const AdminTabNavigator = () => {
  const userStore = useUserStore()
  const navigation = useNavigation()

  return (
    <TabAdmin.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarBadgeStyle: {
          position: "absolute",
          top: 10,
        },
        tabBarStyle: {
          height: Platform.OS === "ios" ? 100 : 70,
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
          alignItems: "center",
          justifyContent: "center",
        },
        headerStyle: { elevation: 0, shadowOpacity: 0 },
        headerRight: () => (
          <TouchableOpacity
            onPress={() => {
              const isLoggedOut = userStore.logout()
              if (isLoggedOut) {
                navigation.navigate("login")
                navigation.reset({
                  index: 0,
                  routes: [{ name: "login" }],
                })
                Toast.show({
                  text1: "Open Eyes Consulting",
                  text2: "You Have Been Successfully Signed Out!",
                  type: "success",
                })
              } else {
                console.log("Error While Signing out")
              }
            }}
            style={{ marginRight: 10, alignItems: "center", justifyContent: "center" }}
          >
            <FontAwesomeIcon icon={faSignOutAlt} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        ),
        headerTitleAlign: "center",
        headerTitleStyle: { fontFamily: "Roboto-Light" },
        headerLeft: () => (
          <View style={{ marginLeft: 10, alignItems: "center", justifyContent: "center" }}>
            <Image
              source={require("../../assets/images/logooec.png")}
              style={{ width: 35, height: 35 }}
              resizeMode="contain"
            />
          </View>
        ),
      }}
    >
      <TabAdmin.Screen
        name="pending"
        component={AdminPendingScreen}
        options={{
          unmountOnBlur: true,
          title: translate("navigatorAdmin.pending"),
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faFileInvoice}
                size={30}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <TabAdmin.Screen
        name="user"
        component={UserTabNavigator}
        options={{
          unmountOnBlur: true,
          title: translate("navigatorAdmin.user"),
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faUser}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <TabAdmin.Screen
        name="commonHoliday"
        component={CongeTabNavigator}
        options={{
          title: translate("navigatorAdmin.commonHoliday"),
          unmountOnBlur: true,
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faCalendar}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <TabAdmin.Screen
        name="notification"
        component={AdminNotificationScreen}
        options={{
          title: translate("navigatorAdmin.notification"),
          unmountOnBlur: true,
          tabBarIcon: ({ focused }) => {
            return <TabBarIcon color={focused ? color.palette.blue_oec : color.palette.lightGrey} />
          },
        }}
      />
    </TabAdmin.Navigator>
  )
}
const HomeTabNavigator = observer(() => {
  const userStore = useUserStore()
  const navigation = useNavigation()
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        tabBarBadgeStyle: {
          position: "absolute",
          top: 10,
        },
        tabBarStyle: {
          height: Platform.OS === "ios" ? 100 : 70,
          borderTopLeftRadius: 25,
          borderTopRightRadius: 25,
        },
        headerRight: () => (
          <TouchableOpacity
            onPress={() => {
              const isLoggedOut = userStore.logout()
              if (isLoggedOut) {
                navigation.navigate("login")
                navigation.reset({
                  index: 0,
                  routes: [{ name: "login" }],
                })
                Toast.show({
                  text1: "Open Eyes Consulting",
                  text2: "You Have Been Successfully Signed Out!",
                  type: "success",
                })
              } else {
                console.log("Error While Signing out")
              }
            }}
            style={{ marginRight: 10, alignItems: "center", justifyContent: "center" }}
          >
            <FontAwesomeIcon icon={faSignOutAlt} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        ),
        headerTitleAlign: "center",
        headerTitleStyle: { fontFamily: "Roboto-Light" },
        headerLeft: () => (
          <View style={{ marginLeft: 10, alignItems: "center", justifyContent: "center" }}>
            <Image
              source={require("../../assets/images/logooec.png")}
              style={{ width: 35, height: 35 }}
              resizeMode="contain"
            />
          </View>
        ),
      }}
    >
      <Tab.Screen
        name="home"
        component={HomeScreen}
        options={{
          title: translate("navigator.home"),
          unmountOnBlur: true,
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faHome}
                size={30}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <Tab.Screen
        name="conge"
        component={CongeScreen}
        options={{
          title: translate("navigator.holidayApplication"),
          unmountOnBlur: true,
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faFileInvoice}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <Tab.Screen
        name="historique"
        component={HistoriqueScreen}
        options={{
          title: translate("navigator.history"),
          unmountOnBlur: true,
          tabBarIcon: ({ focused }) => {
            return (
              <FontAwesomeIcon
                icon={faHistory}
                size={25}
                color={focused ? color.palette.blue_oec : color.palette.lightGrey}
              />
            )
          },
        }}
      />
      <Tab.Screen
        name="notification"
        component={AdminNotificationScreen}
        options={{
          title: translate("navigatorAdmin.notification"),
          unmountOnBlur: true,
          tabBarBadge: userStore.unreadNotification,
          tabBarIcon: ({ focused }) => {
            return <TabBarIcon color={focused ? color.palette.blue_oec : color.palette.lightGrey} />
          },
        }}
      />
    </Tab.Navigator>
  )
})

const AppStack = observer(() => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="login"
    >
      <Stack.Screen name="login" component={LoginScreen} />
      <Stack.Screen name="register" component={RegisterScreen} />
      <Stack.Screen name="home" component={HomeTabNavigator} />
      <Stack.Screen name="admin" component={AdminTabNavigator} />
      <Stack.Screen name="registerSignature" component={RegisterSignatureScreen} />
      <Stack.Screen name="editCommonHoliday" component={AdminEditCommonHolidayScreen} />
    </Stack.Navigator>
  )
})

interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = (props: NavigationProps) => {
  const colorScheme = useColorScheme()
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      <Toast style={{ zIndex: 999 }} ref={(ref) => Toast.setRef(ref)} />
      <AppStack />
    </NavigationContainer>
  )
}

AppNavigator.displayName = "AppNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["welcome"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
