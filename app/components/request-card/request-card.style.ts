/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  cardContainer: {
    alignSelf: "center",
    backgroundColor: color.palette.white,
    borderRadius: 10,
    elevation: 2,
    flexDirection: "row",
    height: "auto",
    margin: 5,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: screenWidth - 30,
  },
  cardContent: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  cardItem: {
    alignItems: "center",
    alignSelf: "flex-start",
    flexDirection: "row",
    flex: 1,
    height: "auto",
    marginBottom: 10,
  },
  itemText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
    fontSize: 16,
    marginHorizontal: 10,
  },
  paiementType: {
    backgroundColor: color.palette.orange,
    height: "100%",
    width: 20,
  },
})
