/* eslint-disable react-native/no-inline-styles */
import React, { FC } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { styles } from "./request-card.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faSignOutAlt,
  faFileSignature,
  faSignInAlt,
  faCircle,
} from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"

export type RequestCardProps = {
  onLongPress?(): void
  onPress?(): void
  data?: { "" }
  width?: 0
}

const RequestCard: FC<RequestCardProps> = (props) => {
  return (
    <TouchableOpacity
      style={styles.cardContainer}
      onPress={() => {
        props.onPress()
      }}
      onLongPress={() => {
        props.onLongPress()
      }}
    >
      <View style={styles.cardContent}>
        <View style={{ flexDirection: "row", justifyContent: "space-between", height: "auto" }}>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faSignOutAlt} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>10/10/2020</Text>
          </View>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faSignInAlt} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>20/10/2020</Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            height: "auto",
            alignItems: "center",
          }}
        >
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faFileSignature} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>Maladie</Text>
          </View>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faCircle} color={color.palette.blue_oec} size={15} />
            <Text style={styles.itemText}>Pending</Text>
          </View>
        </View>
        <View style={styles.cardItem}></View>
      </View>
    </TouchableOpacity>
  )
}

export default RequestCard
