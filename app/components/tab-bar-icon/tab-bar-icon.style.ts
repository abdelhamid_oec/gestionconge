/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native"
import { color } from "../../theme"

export const styles = StyleSheet.create({
  badge: {
    backgroundColor: "red",
    borderRadius: 20,
    height: 20,
    minWidth: 20,
    position: "absolute",
    right: -10,
    top: -10,
    alignItems: "center",
    justifyContent: "center",
  },
  badgeText: {
    color: color.palette.white,
    fontSize: 12,
    fontFamily: "Roboto-Light",
  },
})
