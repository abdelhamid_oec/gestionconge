import React, { FC, useState, useEffect } from "react"
import { View, Text } from "react-native"
import { styles } from "./tab-bar-icon.style"
import { useUserStore } from "../../models/user-store/user-store"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faBell } from "@fortawesome/free-solid-svg-icons"
import { observer } from "mobx-react-lite"

const TabBarIcon: FC = observer((props) => {
  const userStore = useUserStore()

  return (
    <View>
      <FontAwesomeIcon color={props.color} size={25} icon={faBell} />
      {userStore.unreadNotification !== null ? (
        <View style={styles.badge}>
          <Text style={styles.badgeText}>{userStore.unreadNotification}</Text>
        </View>
      ) : null}
    </View>
  )
})

export default TabBarIcon
