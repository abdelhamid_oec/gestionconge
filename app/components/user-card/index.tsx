/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { color } from "../../theme"
import { styles } from "./user-card.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons"
import { Menu, MenuItem, MenuDivider } from "react-native-material-menu"

export type UserCardProps = {
  onDetail?(): void
  onEdit?(): void
  data?: DataProps
  width?: 0
}

export type DataProps = {
  name: string
  surname: string
  email: string
  dateDebut: string
  status: string
}

const UserCard: FC<UserCardProps> = (props) => {
  const [visible, setVisible] = useState(false)

  let statusColor = "#000"
  switch (props.data.status) {
    case "pending":
      statusColor = color.palette.blue_oec
      break
    case "accepted":
      statusColor = color.palette.green_oec
      break
    default:
      statusColor = color.palette.lightGrey
      break
  }

  const hideMenu = () => setVisible(false)

  const showMenu = () => setVisible(true)

  return (
    <View style={styles.cardContainer}>
      <View style={[styles.leftView, { backgroundColor: statusColor }]}></View>
      <View style={styles.cardContent}>
        <View style={{ height: "auto" }}>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>Name : {props.data.name}</Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>Surname : {props.data.surname}</Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>Email : {props.data.email}</Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>Starting Date : {props.data.dateDebut}</Text>
          </View>
        </View>
      </View>
      <View style={styles.iconWrapper}>
        <Menu
          visible={visible}
          anchor={
            <TouchableOpacity onPress={showMenu}>
              <FontAwesomeIcon icon={faEllipsisV} color={color.palette.blue_oec} size={20} />
            </TouchableOpacity>
          }
          onRequestClose={hideMenu}
        >
          <MenuItem
            onPress={() => {
              props.onEdit()
              hideMenu()
            }}
          >
            Edit
          </MenuItem>
          <MenuDivider />
          <MenuItem
            onPress={() => {
              props.onDetail()
              hideMenu()
            }}
          >
            Details
          </MenuItem>
        </Menu>
      </View>
    </View>
  )
}

export default UserCard
