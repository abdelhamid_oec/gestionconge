/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  cardContainer: {
    alignSelf: "center",
    backgroundColor: color.palette.white,
    borderRadius: 5,
    elevation: 2,
    flexDirection: "row",
    height: "auto",
    marginTop: 10,

    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: screenWidth - 25,
  },
  cardContent: {
    flex: 1,
    paddingVertical: 10,
  },
  cardItem: {
    alignItems: "center",
    alignSelf: "flex-start",
    flexDirection: "row",
    flex: 1,
    height: "auto",
  },

  checkText: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
  },
  iconEditWraper: {
    alignItems: "center",
    backgroundColor: color.palette.white,
    flex: 1,
    justifyContent: "center",
  },
  iconWrapper: {
    width: 50,
  },
  itemText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
    fontSize: 12,
  },
  itemTextType: {
    color: color.palette.black,
    fontFamily: "Roboto-Light",
    fontSize: 14,
  },

  leftView: {
    backgroundColor: "#FFC300",
    width: 7.5,
  },
})
