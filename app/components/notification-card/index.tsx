/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState, useEffect } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { styles } from "./notification-card.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faBell, faTimesCircle } from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"
import { faSquare, faCheckSquare } from "@fortawesome/free-regular-svg-icons"
import moment from "moment"
export type NotificationCardProps = {
  onLongPress?(): void
  onPress?(): void
  data?: { "" }
  width?: 0
}

const Notificationcard: FC<NotificationCardProps> = (props) => {
  const [isSeen, setIsSeen] = useState<boolean>(false)

  const markNotificationAsSeen = () => {
    setIsSeen(true)
  }

  useEffect(() => {
    if (props.data.status != 0) {
      setIsSeen(true)
    } else {
      setIsSeen(false)
    }
  }, [])
  return (
    <View style={styles.cardContainer}>
      <View
        style={[styles.leftView, { backgroundColor: isSeen ? color.palette.lightGrey : "#FFC300" }]}
      ></View>

      <View style={styles.iconWrapper}>
        <TouchableOpacity style={styles.iconEditWraper}>
          <FontAwesomeIcon
            icon={faBell}
            color={isSeen ? color.palette.lightGrey : "#FFC300"}
            size={20}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.cardContent}>
        <View style={[styles.cardItem, { marginBottom: 5 }]}>
          <Text style={[styles.itemTextType, {}]}>Vacation Leave Request</Text>
        </View>
        <View style={styles.cardItem}>
          <Text style={[styles.itemText, {}]}>{props.data.notification}</Text>
        </View>
        <View style={[styles.cardItem, { marginTop: 5, alignSelf: "flex-end" }]}>
          <Text style={[styles.itemText, {}]}>{moment(props.data.notif_date).fromNow()}</Text>
        </View>
      </View>

      <View style={styles.iconWrapper}>
        <TouchableOpacity
          style={styles.iconEditWraper}
          onPress={() => {
            markNotificationAsSeen()
          }}
        >
          {isSeen ? (
            <FontAwesomeIcon icon={faCheckSquare} color={color.palette.lightGrey} size={15} />
          ) : (
            <FontAwesomeIcon icon={faSquare} color={color.palette.lightGrey} size={15} />
          )}
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Notificationcard
