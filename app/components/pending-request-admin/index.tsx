/* eslint-disable react-native/no-inline-styles */
import React, { FC } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { styles } from "./pending-request-admin.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faTrash, faCheck, faInfo } from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"
import { translate } from "../../i18n"
export type PendingCardProps = {
  onCheck?(): void
  onDelete?(): void
  onDetail?(): void
  data?: DataProps
  width?: 0
}

export type DataProps = {
  nom: string
  prenom: string
  type: string
  dateDebut: string
  dateFin: string
}

const PendingRequestAdmin: FC<PendingCardProps> = (props) => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardContent}>
        <View style={{ height: "auto" }}>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>
              {translate("registerScreen.name")} : {props.data.nom}
            </Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>
              {translate("registerScreen.surname")} : {props.data.prenom}
            </Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>
              {translate("typeConge.type")} : {props.data.type}
            </Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>
              {translate("typeConge.dateDebut")} : {props.data.dateDebut}
            </Text>
          </View>
          <View style={styles.cardItem}>
            <Text style={styles.itemText}>
              {translate("typeConge.dateFin")} : {props.data.dateFin}
            </Text>
          </View>
        </View>
      </View>

      <View style={styles.iconWrapper}>
        <TouchableOpacity
          onPress={() => {
            props.onCheck()
          }}
          style={styles.iconEditWraper}
        >
          <FontAwesomeIcon icon={faCheck} color={color.palette.white} size={17} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.onDelete()
          }}
          style={styles.iconDeleteWraper}
        >
          <FontAwesomeIcon icon={faTrash} color={color.palette.white} size={17} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.onDetail()
          }}
          style={styles.iconDetailWraper}
        >
          <FontAwesomeIcon icon={faInfo} color={color.palette.white} size={17} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default PendingRequestAdmin
