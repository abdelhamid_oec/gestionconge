/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  cardContainer: {
    alignSelf: "center",
    backgroundColor: color.palette.white,
    borderRadius: 10,
    elevation: 2,
    flexDirection: "row",
    height: "auto",
    marginTop: 10,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: screenWidth - 30,
  },
  cardContent: {
    flex: 3,
    paddingHorizontal: 10,
    paddingVertical: 10,
    paddingBottom: 20,
  },
  cardItem: {
    alignItems: "center",
    alignSelf: "flex-start",
    flexDirection: "row",
    flex: 1,
    height: "auto",
    marginTop: 10,
  },
  iconDeleteWraper: {
    alignItems: "center",
    backgroundColor: "red",
    flex: 1,
    justifyContent: "center",
  },
  iconDetailWraper: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    flex: 1,
    justifyContent: "center",
  },
  iconEditWraper: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    flex: 1,
    justifyContent: "center",
  },
  iconWrapper: {
    flex: 1,
  },
  itemText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
    fontSize: 16,
    marginHorizontal: 10,
  },
})
