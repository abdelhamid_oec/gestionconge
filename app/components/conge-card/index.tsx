import React, { FC } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { styles } from "./conge-card.style"
import { BannerIconComponent } from "../icon/svg/banner-icon"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faSignOutAlt,
  faHandHoldingUsd,
  faFileSignature,
  faSignInAlt,
  faSyncAlt,
} from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"

export type CongeCardProps = {
  onLongPress?(): void
  onPress?(): void
  data?: CongeCardData
  width?: 0
}
export type CongeCardData = {
  startDate: string
  endDate: string
  holidayType: string
  status: string
}

const CongeCard: FC<CongeCardProps> = (props) => {
  return (
    <TouchableOpacity
      style={styles.cardContainer}
      onPress={() => {
        props.onPress()
      }}
      onLongPress={() => {
        props.onLongPress()
      }}
    >
      <View style={styles.paiementType}>
        <BannerIconComponent primaryColor={"#DD352E"} secondaryColor={"#fff"} />
      </View>
      <View style={styles.cardContent}>
        <View style={styles.cardItem}>
          <FontAwesomeIcon icon={faSignOutAlt} color={color.palette.blue_oec} size={20} />
          <Text style={styles.itemText}>{props.data.startDate}</Text>
        </View>
        <View style={styles.cardItem}>
          <FontAwesomeIcon icon={faSignInAlt} color={color.palette.blue_oec} size={20} />
          <Text style={styles.itemText}>{props.data.endDate}</Text>
        </View>
        <View style={styles.cardItem}>
          <FontAwesomeIcon icon={faFileSignature} color={color.palette.blue_oec} size={20} />
          <Text style={styles.itemText}>{props.data.holidayType}</Text>
        </View>
        <View style={styles.cardItem}>
          <FontAwesomeIcon icon={faSyncAlt} color={color.palette.blue_oec} size={20} />
          <Text style={styles.itemText}>{props.data.status}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default CongeCard
