/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"
import { autoAction } from "mobx/dist/internal"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: color.palette.white,
    borderRadius: 10,
    elevation: 2,
    height: "auto",
    margin: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: screenWidth / 2 - 10,
  },
  cardContent: {
    paddingHorizontal: 20,
    paddingVertical: 35,
  },
  cardItem: {
    alignItems: "center",
    flexDirection: "row",
    marginBottom: 10,
  },
  itemText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
    fontSize: 16,
    marginHorizontal: 10,
  },
  paiementType: {
    position: "absolute",
    right: 10,
    top: -0,
  },
})
