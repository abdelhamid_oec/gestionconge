/* eslint-disable react-native/sort-styles */
/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  cardContainer: {
    // alignSelf: "center",
    backgroundColor: color.palette.white,
    borderRadius: 10,
    elevation: 2,
    height: "auto",
    overflow: "hidden",
    marginTop: 5,
    marginRight: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: screenWidth / 2 - 10,
  },
  cardContent: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  cardItem: {
    alignItems: "center",
    alignSelf: "center",
    flexDirection: "row",
    flex: 1,
    height: "auto",
    marginBottom: 15,
  },
  iconDeleteWraper: {
    alignItems: "center",
    backgroundColor: "red",
    flex: 1,
    justifyContent: "center",
  },
  iconDetailWraper: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    flex: 1,
    justifyContent: "center",
  },
  iconEditWraper: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    flex: 1,
    justifyContent: "center",
  },
  iconWrapper: {
    flexDirection: "row",
    height: 50,
  },
  imageStyle: {
    height: 60,
    width: 90,
  },
  itemText: {
    color: color.palette.black,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    marginHorizontal: 10,
    textAlign: "center",
  },
  logoWrapper: {
    alignItems: "center",
    height: "auto",
    justifyContent: "center",
    marginBottom: 10,
    marginTop: 10,
  },
})
