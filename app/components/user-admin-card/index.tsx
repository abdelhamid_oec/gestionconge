/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { FC } from "react"
import { View, Text, TouchableOpacity, Image } from "react-native"
import { styles } from "./user-admin-card.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faTrash, faCheck, faInfo } from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"

const logo = require("../../../assets/images/logooec.png")

export type RequestCardUserProps = {
  onCheck?(): void
  onDelete?(): void
  onDetail?(): void
  data?: DataProps
  width?: 0
}

export type DataProps = {
  name: string
  surname: string
}

const UseradminCard: FC<RequestCardUserProps> = (props) => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardContent}>
        <View style={styles.logoWrapper}>
          <Image style={styles.imageStyle} source={logo}></Image>
        </View>

        <View style={styles.cardItem}>
          <Text style={styles.itemText}>
            {props.data.name} {props.data.surname}
          </Text>
        </View>
      </View>

      <View style={styles.iconWrapper}>
        <TouchableOpacity
          onPress={() => {
            props.onCheck()
          }}
          style={styles.iconEditWraper}
        >
          <FontAwesomeIcon icon={faCheck} color={color.palette.white} size={20} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            props.onDelete()
          }}
          style={styles.iconDeleteWraper}
        >
          <FontAwesomeIcon icon={faTrash} color={color.palette.white} size={20} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.onDetail()
          }}
          style={styles.iconDetailWraper}
        >
          <FontAwesomeIcon icon={faInfo} color={color.palette.white} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default UseradminCard
