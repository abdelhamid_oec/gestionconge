/* eslint-disable react-native/no-inline-styles */
import React, { FC } from "react"
import { View, Text, TouchableOpacity } from "react-native"
import { styles } from "./consult-conge-admin.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faSignOutAlt,
  faSignInAlt,
  faCalendar,
  faPen,
  faTrash,
} from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"

export type RequestCardProps = {
  onEdit?(): void
  onDelete?(): void
  data?: DataProps
  width?: 0
}

export type DataProps = {
  nomHoliday: string
  dateDebut: string
  dateFin: string
}

const ConsultCongeadmin: FC<RequestCardProps> = (props) => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardContent}>
        <View style={{ height: "auto" }}>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faCalendar} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>{props.data.nomHoliday}</Text>
          </View>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faSignOutAlt} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>{props.data.dateDebut}</Text>
          </View>
          <View style={styles.cardItem}>
            <FontAwesomeIcon icon={faSignInAlt} color={color.palette.blue_oec} size={20} />
            <Text style={styles.itemText}>{props.data.dateFin}</Text>
          </View>
        </View>

        <View style={styles.cardItem}></View>
      </View>

      <View style={styles.iconWrapper}>
        <TouchableOpacity
          onPress={() => {
            props.onEdit()
          }}
          style={styles.iconEditWraper}
        >
          <FontAwesomeIcon icon={faPen} color={color.palette.white} size={20} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.onDelete()
          }}
          style={styles.iconDeleteWraper}
        >
          <FontAwesomeIcon icon={faTrash} color={color.palette.white} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default ConsultCongeadmin
