import AsyncStorage from "@react-native-async-storage/async-storage"

export const sendNotificationToDevice = async (device, messageText) => {
  const headers = new Headers({
    "Content-Type": "application/json",
    Authorization:
      "key=" +
      "AAAAZyooY-g:APA91bF7TCtN3mrgevZGWqtpodQ9guLRxfG7UtZ-RrhAfgOstsVI9ro650n6AhoWtywH2bYyvYHMimvu-bZCylvZ9YRcfSUM3kZ0voeSEI5hMyIaFP_7iQ5jQEDRwUQZcH1YyLB8UVKo",
  })

  const message = {
    to: device,
    notification: {
      body: messageText,
      title: "Vacation Leave Request",
      bigPictureUrl: "https://www.farojob.net/wp-content/uploads/2016/12/OpenEyes-Consulting.png",
      largeIconUrl: "https://www.farojob.net/wp-content/uploads/2016/12/OpenEyes-Consulting.png",
    },
  }

  const response = await fetch("https://fcm.googleapis.com/fcm/send", {
    method: "POST",
    headers: headers,
    body: JSON.stringify(message),
  })

  return response.json()
}

export const extractDeviceToken = () => {
  return AsyncStorage.getItem("DEVICE_TOKEN")
    .then((token) => {
      const tokenData = JSON.parse(token)
      return tokenData.token
    })
    .catch((err) => {
      console.log("Error Parsing Item From AsyncStorage:", err)
      return null
    })
}
