import { Api } from "./api"
import moment from "moment"

export type CongeTypeData = {
  idUser: string
  startDate: Date
  endDate: Date
  type: string
  description: string
  signature: string
  name: string
  surname: string
}

export const addConge = async (api: Api, data: CongeTypeData) => {
  try {
    const formData = new FormData()
    formData.append("start_date", moment(data.startDate).format("YYYY/MM/DD").toString())
    formData.append("end_date", moment(data.endDate).format("YYYY/MM/DD").toString())
    formData.append("holiday_type", data.type)
    formData.append("description", data.description)
    formData.append("signature", data.signature)
    formData.append("id_user", data.idUser)
    formData.append("name", data.name)
    formData.append("surname", data.surname)
    const response = await api.apisauce.post("/api/Conge/index.php", formData)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const acceptConge = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/Conge/index.php/accept/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const deleteConge = async (api: Api, id: number, reason: string) => {
  try {
    const formData = new FormData()
    formData.append("reason", reason)
    const response = await api.apisauce.post(`/api/Conge/index.php/delete/${id}`, formData)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const getPendingRequest = async (api: Api) => {
  try {
    const response = await api.apisauce.get("/api/Conge/index.php/pending")
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
  }
}
export const getHistoryByUserID = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.get(`/api/Conge/index.php/request/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error From GetHistoryByID:", error)
    return null
  }
}
