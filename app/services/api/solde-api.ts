import { ApiResponse } from "apisauce"
import { Api } from "./api"
import qs from "qs"
import moment from "moment"

export const getSoldeById = async (api: Api, idUser: number, month: string, year: string) => {
  try {
    const response = await api.apisauce.get(`/api/Solde/index.php/getById/${idUser}`, {
      month: month,
      year: year,
    })
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
  }
}
