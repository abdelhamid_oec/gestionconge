import { Api } from "./api"
import moment from "moment"
import { RegisterData } from "../../screens/Register"

export type UserTypeData = {
  name: string
  surname: string
  identity: string
  personnalEmail: string
  address: string
  email: string
  signature: string
  password: string
  phone: string
  phone2: string
  startWorkingDate: Date
  type: string
}

export const addUser = async (api: Api, data: RegisterData) => {
  try {
    const formData = new FormData()
    formData.append("name", data.name)
    formData.append("surname", data.surname)
    formData.append("identity", data.identity)
    formData.append("personal_email", data.personnalEmail)
    formData.append("address", data.address)
    formData.append("email", data.email)
    formData.append("signature", data.signature)
    formData.append("password", data.password)
    formData.append("primary_phone", data.phoneNumber)
    formData.append("secondary_phone", data.optionnalPhoneNumber)
    formData.append("type", data.type)
    formData.append("starting_date", moment(data.startWorkingDate).format("YYYY/MM/DD").toString())
    console.log("Date:", moment(data.startWorkingDate).format("YYYY/MM/DD").toString())
    const response = await api.apisauce.post("/api/User/index.php/register", formData)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const getAllUsers = async (api: Api) => {
  try {
    const response = await api.apisauce.get("/api/User/index.php")
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const getSpecificUsers = async (api: Api) => {
  try {
    const response = await api.apisauce.get("/api/User/index.php/specific")
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const getPendingUsers = async (api: Api) => {
  try {
    const response = await api.apisauce.get("/api/User/index.php/pending")
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const acceptUser = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/User/index.php/accept/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const deactivateUser = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/User/index.php/deactivate/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const deleteUser = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/User/index.php/delete/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const login = async (api: Api, email: string, password: string) => {
  try {
    const formData = new FormData()
    formData.append("email", email)
    formData.append("password", password)
    console.log(formData)
    const response = await api.apisauce.post("/api/User/index.php/login", formData)
    return response.data
  } catch (error) {
    console.log("Login Error:", error)
  }
}

export const updateDeviceToken = async (api: Api, id: number, token: string) => {
  try {
    const formData = new FormData()
    formData.append("token", token)
    const response = await api.apisauce.post(`/api/User/index.php/token/${id}`, formData)
    return response.data
  } catch (error) {
    console.log("Error While Updating Device Token:", error)
  }
}

export const getNotificationsById = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.get(`/api/Notification/index.php/notifications/${id}`)
    return response.data
  } catch (error) {
    console.log("Error While Retreiving Notification:", error)
  }
}

export const markNotificationAsSeen = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/Notification/index.php/seen/${id}`)
    return response.data
  } catch (error) {
    console.log("Error While Marking Notification as Seen:", error)
  }
}
