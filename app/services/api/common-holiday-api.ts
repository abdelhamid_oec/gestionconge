import { Api } from "./api"
import moment from "moment"

export type CommonHolidayTypeData = {
  name: string
  startDate: Date
  endDate: Date
  description: string
  type: string
}

export const addCommonHoliday = async (api: Api, data: CommonHolidayTypeData) => {
  try {
    const formData = new FormData()
    formData.append("name", data.name)
    formData.append("start_date", moment(data.startDate).format("YYYY/MM/DD").toString())
    formData.append("end_date", moment(data.endDate).format("YYYY/MM/DD").toString())
    formData.append("description", data.description)
    formData.append("type", data.type)

    const response = await api.apisauce.post("/api/CommonHoliday/index.php/insert", formData)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const getAllCommonHoliday = async (api: Api) => {
  try {
    const response = await api.apisauce.get(`/api/CommonHoliday/index.php/getAll`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const deleteCommonHoliday = async (api: Api, id: number) => {
  try {
    const response = await api.apisauce.post(`/api/CommonHoliday/index.php/delete/${id}`)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}

export const updateCommonHoliday = async (api: Api, id: number, data: CommonHolidayTypeData) => {
  try {
    const formData = new FormData()
    formData.append("name", data.name)
    formData.append("start_date", moment(data.startDate).format("YYYY/MM/DD").toString())
    formData.append("end_date", moment(data.endDate).format("YYYY/MM/DD").toString())
    formData.append("description", data.description)
    formData.append("type", data.type)
    const response = await api.apisauce.post(`/api/CommonHoliday/index.php/update/${id}`, formData)
    const result = response.data
    return result
  } catch (error) {
    console.log("Error :", error)
    return null
  }
}
