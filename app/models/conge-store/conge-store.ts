import { createContext, useContext } from "react"
import { makeObservable, observable, action } from "mobx"
import {
  acceptConge,
  addConge,
  CongeTypeData,
  deleteConge,
  getPendingRequest,
  getHistoryByUserID,
} from "../../services/api/conge-api"
import { Api } from "../../services/api"
class CongeStore {
  pendingRequest = []

  userHistory = []
  constructor() {
    makeObservable(this, {
      userHistory: observable,
      addConge: action.bound,
      pendingRequest: observable,
      acceptConge: action.bound,
      getPendingRequest: action.bound,
      deleteConge: action.bound,
      getHistoryById: action.bound,
      setUserHistory: action.bound,
      setPendingRequest: action.bound,
    })
  }

  public setPendingRequest(pendingRequest): void {
    this.pendingRequest = pendingRequest
  }

  public async addConge(api: Api, data: CongeTypeData): Promise<any> {
    const result = await addConge(api, data)
    return result
  }

  public async acceptConge(api: Api, id: number): Promise<any> {
    const result = await acceptConge(api, id)
    return result
  }

  public async deleteConge(api: Api, id: number, reason: string): Promise<any> {
    const result = await deleteConge(api, id, reason)
    return result
  }

  public async getPendingRequest(api: Api): Promise<any> {
    const result = await getPendingRequest(api)
    return result
  }

  public async getHistoryById(api: Api, id: number): Promise<any> {
    const result = await getHistoryByUserID(api, id)
    return result
  }

  public setUserHistory(userHistory): void {
    this.userHistory = userHistory
  }
}

const congeStore = new CongeStore()

export const congeContext = createContext(congeStore)

export const useCongeStore = () => useContext(congeContext)
