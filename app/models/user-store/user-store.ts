import { useContext, createContext } from "react"
import { makeObservable, observable, action } from "mobx"
import {
  updateDeviceToken,
  getNotificationsById,
  markNotificationAsSeen,
  acceptUser,
  getAllUsers,
  getPendingUsers,
  login,
  deleteUser,
  getSpecificUsers,
  addUser,
  deactivateUser,
} from "../../services/api/user-api"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { Alert } from "react-native"
import { RegisterData } from "../../screens/Register"

export class UserStore {
  user: any = {}
  users: any = []
  pendingUser: any = []
  notifications = []
  unreadNotification = null

  constructor() {
    makeObservable(this, {
      user: observable,
      pendingUser: observable,
      notifications: observable,
      unreadNotification: observable,
      Login: action.bound,
      setUser: action.bound,
      setNotifications: action.bound,
      setUnreadNotifcation: action.bound,
      logout: action.bound,
      updateToken: action.bound,
      getNotifications: action.bound,
      updateNotifications: action.bound,
      getAllUsers: action.bound,
      getpendingUsers: action.bound,
      acceptUser: action.bound,
      deleteUser: action.bound,
      getSpecificUsers: action.bound,
      addUser: action.bound,
      deactivateUser: action.bound,
      setUsers: action.bound,
      setPendingUser: action.bound,
    })
  }

  public async Login(api, email, password): Promise<any> {
    const result = await login(api, email, password)
    if (result.status === 200) {
      AsyncStorage.setItem("token", result.data.token)
        .then((response) => {
          console.log("Item Successfully set to Async Storage:", result.data.token)
        })
        .catch((err) => {
          console.log("From AsyncStorage:", err)
        })
      return result
    } else {
      Alert.alert("Login", result.data.msg)
      return {}
    }
  }

  public async addUser(api, data: RegisterData): Promise<any> {
    const result = await addUser(api, data)
    return result
  }

  public setUser(user) {
    this.user = user
  }

  public setPendingUser(pendingUser) {
    this.pendingUser = pendingUser
  }

  public setUsers(users) {
    this.users = users
  }

  public async acceptUser(api, id: number): Promise<any> {
    const result = await acceptUser(api, id)
    return result
  }

  public async deactivateUser(api, id: number): Promise<any> {
    const result = await deactivateUser(api, id)
    return result
  }

  public async deleteUser(api, id: number): Promise<any> {
    const result = await deleteUser(api, id)
    return result
  }

  public async getAllUsers(api): Promise<any> {
    const result = await getAllUsers(api)
    return result
  }

  public async getSpecificUsers(api): Promise<any> {
    const result = await getSpecificUsers(api)
    return result
  }

  public async getpendingUsers(api): Promise<any> {
    const result = await getPendingUsers(api)
    return result
  }

  public setUnreadNotifcation(number) {
    this.unreadNotification = number
  }

  public setNotifications(notifications) {
    this.notifications = notifications
  }

  public async logout() {
    await AsyncStorage.removeItem("token", () => {
      this.setUser({})
    })
    return true
  }

  public async updateToken(api, id, token): Promise<any> {
    const result = await updateDeviceToken(api, id, token)
    if (result.status === 200) {
      return result
    } else {
      return {}
    }
  }

  public async getNotifications(api, id): Promise<any> {
    const result = await getNotificationsById(api, id)
    if (result.status === 200) {
      return result.data
    } else {
      return []
    }
  }

  public async updateNotifications(api, id): Promise<any> {
    const result = await markNotificationAsSeen(api, id)
    return result
  }
}
const userStore = new UserStore()
export const userStoreContext = createContext(userStore)
export const useUserStore = () => useContext(userStoreContext)
