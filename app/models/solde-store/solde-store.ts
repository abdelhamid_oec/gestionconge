import React, { useContext, createContext } from "react"
import { makeObservable, observable, action } from "mobx"
import { getSoldeById } from "../../services/api/solde-api"

class SoldeStore {
  solde: any = {}

  constructor() {
    makeObservable(this, {
      solde: observable,
      getSoldeById: action.bound,
      setSolde: action.bound,
    })
  }

  public async getSoldeById(api, userId, month, year): Promise<any> {
    const result = await getSoldeById(api, userId, month, year)
    console.log("Store:", result)
    if (result.status === 200) {
      return result
    } else {
      console.log("Error From Solde Store")
      return {}
    }
  }

  public setSolde(solde) {
    this.solde = solde
  }
}

/*

public async getSoldeById(api, userId):Promise<any> {
    getSoldeById(api, userId)
      .then((response) => {
        if (response.status == 200) {
          this.solde = response.data
        } else {
          console.log(response.data)
        }
      })
      .catch((err) => {
        console.log("Error Produced by getSoldeById:", err)
      })
  }

  */
const soldeStore = new SoldeStore()

export const soldeStoreContext = createContext(soldeStore)
export const useSoldeStore = () => useContext(soldeStoreContext)
