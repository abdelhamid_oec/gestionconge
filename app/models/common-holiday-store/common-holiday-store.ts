import { createContext, useContext } from "react"
import { makeObservable, observable, action } from "mobx"

import { Api } from "../../services/api"
import {
  addCommonHoliday,
  CommonHolidayTypeData,
  deleteCommonHoliday,
  getAllCommonHoliday,
  updateCommonHoliday,
} from "../../services/api/common-holiday-api"

class CommonHolidayStore {
  commonHolidayData = []
  constructor() {
    makeObservable(this, {
      addCommonHoliday: action.bound,
      commonHolidayData: observable,
      deleteCommonHoliday: action.bound,
      getAllCommonHoliday: action.bound,
      setCommonHolidayData: action.bound,
    })
  }

  public setCommonHolidayData(commonHolidayData) {
    this.commonHolidayData = commonHolidayData
  }

  public async addCommonHoliday(api: Api, data: CommonHolidayTypeData): Promise<any> {
    const result = await addCommonHoliday(api, data)
    return result
  }

  public async getAllCommonHoliday(api: Api): Promise<any> {
    const result = await getAllCommonHoliday(api)
    return result
  }

  public async deleteCommonHoliday(api: Api, id: number): Promise<any> {
    const result = await deleteCommonHoliday(api, id)
    return result
  }

  public async updateCommonHoliday(
    api: Api,
    id: number,
    data: CommonHolidayTypeData,
  ): Promise<any> {
    const result = await updateCommonHoliday(api, id, data)
    return result
  }
}

const commonHolidayStore = new CommonHolidayStore()

export const commonHolidayContext = createContext(commonHolidayStore)

export const usecommonHolidayStore = () => useContext(commonHolidayContext)
