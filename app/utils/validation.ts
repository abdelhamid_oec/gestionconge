/* eslint-disable no-useless-escape */
/* eslint-disable prefer-regex-literals */
/* eslint-disable no-invalid-regexp */
import { validate } from "react-email-validator"
export const validateEmail = (value: string) => {
  return validate(value)
}

export const validatePassword = (value: string) => {
  if (value.length < 8 || value.length > 16) {
    return {
      error: true,
      msg: "Password must be 8-16 long",
    }
  } else {
    return {
      error: false,
      msg: "",
    }
  }
}

export const validateName = (value: string) => {
  if (value.length < 3 || value.length > 20) {
    return {
      error: true,
      msg: "Name must be 3-16 long",
    }
  } else {
    return {
      error: false,
      msg: "",
    }
  }
}

export const validateAddress = (value: string) => {
  if (value.length < 5 || value.length > 20) {
    return {
      error: true,
      msg: "Address must be 3-16 long",
    }
  } else {
    return {
      error: false,
      msg: "",
    }
  }
}

export const validatePhone = (value: string) => {
  if (value.length !== 8) {
    return {
      error: true,
      msg: "Phone must be 8 long",
    }
  } else {
    return {
      error: false,
      msg: "",
    }
  }
}
