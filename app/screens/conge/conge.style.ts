/* eslint-disable react-native/no-color-literals */

import { Dimensions, StyleSheet } from "react-native"
import { color } from "../../theme"
const screenWidth = Dimensions.get("screen").width
export const styles = StyleSheet.create({
  buttonStyle: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    borderRadius: 20,
    height: 60,
    justifyContent: "center",
    width: screenWidth - 60,
  },
  buttonText: {
    color: color.palette.white,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  dateHolder: {
    alignItems: "center",
    borderColor: "rgba(0,0,0,0.1)",
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: "row",
    fontFamily: "Roboto-Light",
    height: 60,
    justifyContent: "space-between",
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  formContainer: {
    alignItems: "center",
    paddingBottom: 25,
    paddingTop: 25,
  },
  formInput: {
    borderColor: "rgba(0,0,0,0.1)",
    borderRadius: 20,
    borderWidth: 1,
    fontFamily: "Roboto-Light",
    height: 60,
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  headerTitle: {
    color: color.palette.blue_oec,
    fontFamily: "Roboto-Bold",
    fontSize: 18,
    marginBottom: 25,
  },
  mainContainer: {
    flex: 1,
  },
  priceStyle: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 25,
    paddingHorizontal: 50,
    width: "100%",
  },
  priceTitle: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  priceTotal: {
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },

  signature: {
    borderColor: "rgba(0,0,0,0.1)",
    height: Dimensions.get("screen").height / 2,
    overflow: "hidden",
    backgroundColor: "red",
  },

  signatureShowButton: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    flex: 1,
    justifyContent: "center",
    width: 150,
  },
  signatureStyle: {
    alignItems: "center",
    borderRadius: 25,
    flexDirection: "row",
    height: 50,
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 10,
    width: Dimensions.get("screen").width - 50,
  },
  signatureText: {
    color: color.palette.white,
    fontFamily: "Roboto-Light",
  },
  signatureTitle: {
    fontFamily: "Roboto-Light",
    fontSize: 14,
    marginVertical: 15,
  },
  signatureModal: {
    padding: 25,
    height: "auto",
    backgroundColor: color.palette.white,
  },
  sginaturebuttonStyle: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    borderRadius: 20,
    height: 60,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
    marginTop: 10,
  },
  title: {
    color: color.palette.black,
    fontFamily: "Roboto-Light",
    marginBottom: 10,
  },
  titleContainer: {
    alignContent: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    marginBottom: 10,
  },
})

export const style = `.m-signature-pad {height: 80vh;width:85vh} .m-signature-pad--footer {
  position: absolute;
  left: 0;
  right: 0;
  bottom: 20px;
  height: 40px;
}`
