/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
import React, { FC, useRef, useState, useEffect } from "react"
import { View, Text, ScrollView, TextInput, ActivityIndicator, Pressable } from "react-native"
import RNPickerSelect from "react-native-picker-select"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import { styles, style } from "./conge.style"
import { TouchableOpacity } from "react-native-gesture-handler"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faCalendar, faTimesCircle } from "@fortawesome/free-solid-svg-icons"
import { color } from "../../theme"

import SignatureScreen, { SignatureViewRef } from "react-native-signature-canvas"
import { translate } from "../../i18n"
import { CongeTypeData } from "../../services/api/conge-api"
import { useUserStore } from "../../models/user-store/user-store"
import { useCongeStore } from "../../models/conge-store/conge-store"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import Modal from "react-native-modal"
import moment from "moment"
import { sendNotificationToDevice } from "../../services/notification"

const CongeScreen: FC = () => {
  const [startDate, setStartDate] = useState<Date>(new Date())
  const [endDate, setEndDate] = useState<Date>(new Date())
  const [startDateText, setStartDateText] = useState<string>("")
  const [endDateText, setEndDateText] = useState<string>("")
  const [isDateOpened, setIsDateOpened] = useState<boolean>(false)
  const [isEndDateOpened, setIsEndDateOpened] = useState<boolean>(false)
  const [typeConge, setTypeConge] = useState<string>(null)
  const [description, setDescription] = useState<string>("")
  const [signature, setSignature] = useState<string>("")
  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const [isSignatureModalOpen, setIsSignatureModalOpen] = useState<boolean>(false)
  const [isSignatureAssigned, setIsSignatureAssigned] = useState<boolean>(false)

  const userStore = useUserStore()
  const congeStore = useCongeStore()

  const ref = useRef<SignatureViewRef>()

  const handleOK = (signature) => {
    console.log("Signature:", signature)
    setSignature(signature)
    setIsSignatureAssigned(true)
    setIsSignatureModalOpen(false)
  }

  const handleEmpty = () => {
    console.log("Empty")
    console.log("Signature:", signature)
  }

  const handleClear = () => {
    setIsSignatureAssigned(false)
    setSignature("")
    console.log("clear success!")
  }

  const handleEnd = () => {}

  const handleData = (data) => {
    console.log("From Handle Data:", data)
  }

  const handleAdd = () => {
    if (startDateText === "") {
      Toast.show({
        type: "error",
        text1: "Vacation Leave Request",
        text2: "Starting date is required",
        topOffset: 60,
      })
    } else if (endDateText === "") {
      Toast.show({
        type: "error",
        text1: "Vacation Leave Request",
        text2: "Ending date is required",
        topOffset: 60,
      })
    } else if (moment(startDate) > moment(endDate)) {
      Toast.show({
        type: "error",
        text1: "Vacation Leave Request",
        text2: "Ending date should be in the future",
        topOffset: 60,
      })
    } else if (typeConge === null) {
      Toast.show({
        type: "error",
        text1: "Vacation Leave Request",
        text2: "Holiday type is required",
        topOffset: 60,
      })
    } else if (signature === "") {
      Toast.show({
        type: "error",
        text1: "Vacation Leave Request",
        text2: "Signature is required",
        topOffset: 60,
      })
    } else {
      setIsAddLoading(true)
      const api = new Api()
      const data: CongeTypeData = {
        idUser: userStore.user.id_user,
        startDate: startDate,
        endDate: endDate,
        type: typeConge,
        description: description,
        signature: signature,
        name: userStore.user.name,
        surname: userStore.user.surname,
      }
      congeStore.addConge(api, data).then((response) => {
        console.log("Response:", response)
        if (response.status === 200) {
          Toast.show({
            type: "success",
            text1: "Holiday Request",
            text2: response.data,
            topOffset: 60,
          })
          resetFormData()
          /*
          userStore
            .getAdminToken(api)
            .then((result) => {
              console.log("Admin Token:", result.data.device_token)
              sendNotificationToDevice(
                result.data.device_token,
                `${userStore.user.name} ${userStore.user.surname} has submitted a holiday request`,
              )
              console.log("Notification Sent")
            })
            .catch((err) => {
              console.log("Error While Sending Notification to admin side:", err)
            })
*/
          setIsAddLoading(false)
        } else {
          Toast.show({
            type: "error",
            text1: "Holiday Request",
            text2: response.data,
            topOffset: 60,
          })

          setIsAddLoading(false)
        }
      })
    }
  }

  const resetFormData = () => {
    setEndDate(new Date())
    setEndDateText("")
    setStartDate(new Date())
    setStartDateText("")
    setSignature("")
    setIsSignatureAssigned(false)
    setTypeConge(null)
    setDescription("")
  }

  useEffect(() => {
    resetFormData()
  }, [])
  return (
    <ScrollView style={styles.mainContainer}>
      <Modal isVisible={isSignatureModalOpen}>
        <View style={styles.signatureModal}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Signature</Text>
            <Pressable
              onPress={() => {
                setIsSignatureModalOpen(!isSignatureModalOpen)
              }}
            >
              <FontAwesomeIcon icon={faTimesCircle} color={color.palette.lightGrey} size={25} />
            </Pressable>
          </View>
          <View style={styles.signature}>
            <SignatureScreen
              ref={ref}
              onEnd={handleEnd}
              onOK={handleOK}
              onEmpty={handleEmpty}
              onClear={handleClear}
              autoClear={false}
              descriptionText={"Signature"}
              webStyle={style}
            />
          </View>
        </View>
      </Modal>

      <View style={styles.formContainer}>
        <DateTimePickerModal
          mode="date"
          isVisible={isDateOpened}
          onConfirm={(startDate) => {
            setIsDateOpened(false)
            setStartDate(startDate)
            setStartDateText(startDate.toDateString())
          }}
          onCancel={() => {
            setIsDateOpened(false)
          }}
        />

        <DateTimePickerModal
          mode="date"
          isVisible={isEndDateOpened}
          onConfirm={(date) => {
            setIsEndDateOpened(false)
            setEndDate(date)
            setEndDateText(date.toDateString())
          }}
          onCancel={() => {
            setIsEndDateOpened(false)
          }}
        />
        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateDebut", {})}
            editable={false}
            value={startDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.green_oec} />
          </TouchableOpacity>
        </View>

        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateFin")}
            editable={false}
            value={endDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsEndDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.green_oec} />
          </TouchableOpacity>
        </View>

        <View style={[styles.formInput, { justifyContent: "center" }]}>
          <RNPickerSelect
            value={typeConge}
            style={{
              inputAndroid: { color: "black" },
            }}
            onValueChange={(value) => {
              setTypeConge(value)
              console.log("Type:", value)
            }}
            items={[
              { label: "maladie", value: "maladie" },
              { label: "Congé", value: "conge" },
              { label: "Autres", value: "autres" },
            ]}
          ></RNPickerSelect>
        </View>
        <TextInput
          style={[styles.formInput, { height: 100, textAlignVertical: "top", paddingTop: 10 }]}
          placeholder={translate("congeScreen.description")}
          editable={true}
          value={description}
          onChangeText={(text) => {
            setDescription(text)
            console.log("Desc:", description)
          }}
          multiline
        />
        <View style={styles.signatureStyle}>
          <Text style={styles.signatureTitle}>Signature</Text>
          {isSignatureAssigned ? (
            <Text>Signature Accuired</Text>
          ) : (
            <TouchableOpacity
              onPress={() => {
                setIsSignatureModalOpen(!isSignatureModalOpen)
              }}
              style={styles.signatureShowButton}
            >
              <Text style={styles.signatureText}>Sign</Text>
            </TouchableOpacity>
          )}
        </View>
        {isAddLoading ? (
          <ActivityIndicator size="large" color={color.palette.green_oec} />
        ) : (
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => {
              handleAdd()
            }}
          >
            <Text style={styles.buttonText}>{translate("congeScreen.addButton")}</Text>
          </TouchableOpacity>
        )}
      </View>
    </ScrollView>
  )
}

export default CongeScreen
