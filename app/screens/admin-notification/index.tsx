/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React, { FC, useEffect } from "react"
import { FlatList, View, Text } from "react-native"
import { styles } from "./admin-notification.style"
import Notificationcard from "../../components/notification-card"
import { useUserStore } from "../../models/user-store/user-store"
import { observer } from "mobx-react-lite"
import { Api } from "../../services/api"

const AdminNotificationScreen: FC = observer(() => {
  const sheetRef = React.useRef(null)
  const userStore = useUserStore()
  const api = new Api()
  useEffect(() => {
    userStore
      .getNotifications(api, userStore.user.id_user)
      .then((result) => {
        if (result.length !== 0) {
          userStore.setNotifications(result)
          const unreadNotifications = result.filter((elm) => elm.status == 0)
          if (unreadNotifications.length !== 0) {
            userStore.setUnreadNotifcation(unreadNotifications.length)
          } else {
            userStore.setUnreadNotifcation(null)
          }
        } else {
          userStore.setNotifications([])
          userStore.setUnreadNotifcation(null)
        }
      })
      .catch((err) => {
        console.log("Error Occured While Retreiving Notifications:", err)
      })

    return () => {
      userStore.updateNotifications(api, userStore.user.id_user).then((result) => {
        if (result.status === 200) {
          if (userStore.unreadNotification !== null) {
            console.log(result.data)
            userStore.setUnreadNotifcation(null)
          }
        } else {
          console.log(result.data)
        }
      })
    }
  }, [])
  return (
    <View style={styles.mainContainer}>
      {userStore.notifications.length !== 0 ? (
        <FlatList
          numColumns={1}
          data={userStore.notifications}
          contentContainerStyle={{ paddingBottom: 10 }}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <Notificationcard data={item} onPress={() => sheetRef.current.snapTo(0)} />
          )}
        />
      ) : (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No Notification Available</Text>
        </View>
      )}
    </View>
  )
})

export default AdminNotificationScreen
