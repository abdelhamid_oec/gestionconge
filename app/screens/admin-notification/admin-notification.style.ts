/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native"
import { color } from "../../theme"
import { fontFamily } from "styled-system"

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  emptyText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
  },
})
