/* eslint-disable react-native/no-inline-styles */
import React, { FC, useEffect, useState, useLayoutEffect } from "react"
import { View, FlatList, ActivityIndicator, Text } from "react-native"
import { styles } from "./historique.style"
import CongeCard, { CongeCardData } from "../../components/conge-card"
import RNPickerSelect from "react-native-picker-select"
import { color } from "../../theme"
import { useUserStore } from "../../models/user-store/user-store"
import { useCongeStore } from "../../models/conge-store/conge-store"
import { Api } from "../../services/api"

const HistoriqueScreen: FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [requestsData, setRequestsData] = useState<any>([])
  const userStore = useUserStore()
  const congeStore = useCongeStore()

  const Capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }

  const getHistoryByUserId = () => {
    const api = new Api()
    const userId = userStore.user.id_user

    return congeStore
      .getHistoryById(api, userId)
      .then((response) => {
        if (response.status === 200) {
          congeStore.setUserHistory(response.data)
          setRequestsData(congeStore.userHistory)
          return false
        } else {
          console.log("Error From FrontEnd Conge")
          return true
        }
      })
      .catch((err) => {
        console.error(err)
        return true
      })
  }

  const handleFilter = (filter) => {
    switch (filter) {
      case "pending": {
        const result = congeStore.userHistory.filter((request) => request.status === "pending")
        setRequestsData(result)
        break
      }
      case "accepted": {
        const result = congeStore.userHistory.filter((request) => request.status === "accepted")
        setRequestsData(result)
        console.log(result.length)
        break
      }
      default: {
        setRequestsData(congeStore.userHistory)
        break
      }
    }
  }

  useEffect(() => {
    setIsLoading(true)
    getHistoryByUserId().then((isDone) => {
      setIsLoading(isDone)
    })
    const interval = setInterval(() => {
      getHistoryByUserId()
        .then((isDone) => {
          return true
        })
        .catch((err) => {
          console.log("Error from interval:", err)
        })
    }, 10000)
    return () => {
      clearInterval(interval)
    }
  }, [])

  return (
    <View style={styles.mainContainer}>
      {isLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator color={color.palette.blue_oec} size="large" />
        </View>
      ) : (
        <View style={styles.mainContainer}>
          <View style={[styles.filterContainer, { justifyContent: "center" }]}>
            <RNPickerSelect
              onValueChange={(value) => handleFilter(value)}
              style={{
                inputAndroid: { color: "black" },
              }}
              items={[
                { label: "Pending", value: "pending", color: color.palette.blue_oec },
                { label: "Accepted", value: "accepted", color: color.palette.blue_oec },
              ]}
            ></RNPickerSelect>
          </View>
          {requestsData.length === 0 ? (
            <View style={styles.noResult}>
              <Text style={styles.noResultText}>No History found</Text>
            </View>
          ) : (
            <FlatList
              data={requestsData}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={{ paddingHorizontal: 5 }}
              renderItem={({ item }) => {
                const data: CongeCardData = {
                  startDate: item.start_date,
                  endDate: item.end_date,
                  holidayType: Capitalize(item.holiday_type),
                  status: Capitalize(item.status),
                }
                return <CongeCard data={data} />
              }}
            />
          )}
        </View>
      )}
    </View>
  )
}

export default HistoriqueScreen
