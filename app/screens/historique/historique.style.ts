/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"
import { fontFamily } from "styled-system"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  filterContainer: {
    alignSelf: "center",
    borderColor: color.palette.blue_oec,
    borderRadius: 20,
    borderWidth: 1,
    fontFamily: "Roboto-Light",
    height: 60,
    marginVertical: 25,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  loading: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  mainContainer: {
    flex: 1,
  },
  noResult: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  noResultText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
  },
})
