/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState, useEffect } from "react"
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  ImageBackground,
  Image,
  Switch,
  Alert,
  StatusBar,
  ActivityIndicator,
} from "react-native"
import { Style } from "./login.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faUser, faLock } from "@fortawesome/free-solid-svg-icons"
import { translate } from "../../i18n"
import { useNavigation } from "@react-navigation/core"
import { color } from "../../theme"
import { Api } from "../../services/api"
import { validateEmail, validatePassword } from "../../utils/validation"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { useUserStore } from "../../models/user-store/user-store"
import { observer } from "mobx-react-lite"
import { extractDeviceToken } from "../../services/notification"

const image = require("../../../assets/images/bg.jpeg")
const logo = require("../../../assets/images/logooec.png")

const LoginScreen: FC = observer(() => {
  const [isEnabled, setIsEnabled] = useState<boolean>(false)
  const [userNameFoccus, setuserNameFoccus] = useState<boolean>(false)
  const [passwordFoccus, setpasswordFoccus] = useState<boolean>(false)
  const [userName, setUserName] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const userStore = useUserStore()
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState)
  const [isLoggingIn, setIsLoggingIn] = useState<boolean>(false)

  const handleAllUser = () => {
    const api = new Api()
  }

  const handleLogin = (email, password) => {
    const api = new Api()
    const passvalidation = validatePassword(password)
    if (validateEmail(email) === false) {
      Alert.alert("Login", "Please enter a valid email")
    } else if (passvalidation.error === true) {
      Alert.alert("Login", passvalidation.msg)
    } else {
      setIsLoggingIn(true)
      userStore
        .Login(api, email, password)
        .then((response) => {
          if (response.data.user != null) {
            console.log("Hello:", response.data.user)
            userStore.setUser(response.data.user)
            extractDeviceToken().then((token) => {
              userStore.updateToken(api, response.data.user.id_user, token).then((result) => {
                if (result.status == 200) {
                  const newUser = response.data.user
                  newUser.device_token = token
                  userStore.setUser(newUser)
                  if (response.data.user.type === "admin") {
                    navigation.navigate("admin")
                    navigation.reset({
                      index: 0,
                      routes: [{ name: "admin" }],
                    })
                  } else {
                    navigation.navigate("home")
                    navigation.reset({
                      index: 0,
                      routes: [{ name: "home" }],
                    })
                  }
                } else {
                  console.log("Something went wrong")
                }
              })
            })

            setIsLoggingIn(false)
          } else {
            console.log("From LoginFront End: Something Went Wrong")
            setIsLoggingIn(false)
          }
        })
        .catch((err) => {
          console.log("From Login FrontEnd Error:", err)
          setIsLoggingIn(false)
        })
    }
  }

  useEffect(() => {
    AsyncStorage.getItem("token")
      .then((token) => {
        if (token == null) {
          navigation.navigate("login")
        }
      })
      .catch((err) => {
        console.log("Failed:", err)
      })
  }, [])
  const navigation = useNavigation()

  return (
    <ImageBackground source={image} resizeMode="cover" blurRadius={15} style={Style.image}>
      <StatusBar backgroundColor={"black"} />
      <View style={Style.logoWrapper}>
        <Image source={logo}></Image>
      </View>

      <View>
        <View style={userNameFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={userNameFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faUser}
              size={20}
              color={userNameFoccus ? color.palette.green_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            keyboardType={"email-address"}
            onChangeText={(text) => {
              setUserName(text)
            }}
            value={userName}
            placeholder={translate("logInScreen.userName")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setuserNameFoccus(true)
            }}
            onBlur={() => {
              setuserNameFoccus(false)
            }}
          />
        </View>

        <View style={passwordFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={passwordFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faLock}
              size={20}
              color={passwordFoccus ? color.palette.green_oec : color.palette.white}
            />
          </View>
          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setPassword(text)
            }}
            keyboardType={"number-pad"}
            value={password}
            placeholder={translate("logInScreen.password")}
            secureTextEntry={true}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setpasswordFoccus(true)
            }}
            onBlur={() => {
              setpasswordFoccus(false)
            }}
          />
        </View>

        <View style={Style.switchWrapper}>
          <Text style={Style.textSwitch}>{translate("logInScreen.rememberMe")}</Text>
          <Switch
            trackColor={{ false: color.palette.dark_grey, true: color.palette.green_oec }}
            thumbColor={color.palette.white}
            ios_backgroundColor={color.palette.dark_grey}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
        {isLoggingIn ? (
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <ActivityIndicator color={color.palette.green_oec} size="large" />
          </View>
        ) : (
          <View>
            <TouchableOpacity
              style={Style.buttonWrapper}
              onPress={() => {
                handleLogin(userName, password)
              }}
            >
              <Text style={Style.textButton}>{translate("logInScreen.signIn")}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={Style.buttonWrapperSignUp}
              onPress={() => {
                navigation.navigate("register")
              }}
            >
              <Text style={Style.textButton}>{translate("logInScreen.create")}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </ImageBackground>
  )
})

export default LoginScreen
