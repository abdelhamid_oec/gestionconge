/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native"
import { color } from "../../theme"

export const styles = StyleSheet.create({
  conge: {
    alignItems: "center",
    borderBottomColor: "rgba(0,0,0,0.1)",
    borderBottomWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
  congeContainer: {
    marginBottom: 25,
    paddingHorizontal: 10,
  },
  congeContent: {
    backgroundColor: "white",
  },
  congeDays: {
    flex: 1,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    textAlign: "center",
  },
  congeHeader: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 15,
  },
  congeTitle: {
    flex: 1,
    fontFamily: "Roboto-Light",
    fontSize: 16,
    textAlign: "center",
  },
  filterIcon: {
    alignItems: "center",
    elevation: 5,
    height: 40,
    justifyContent: "center",
    width: 40,
  },
  headerText: {
    color: "white",
    flex: 1,
    textAlign: "center",
  },
  loadingStyle: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  mainContainer: {
    flex: 1,
  },
  noResult: {
    alignItems: "center",
    justifyContent: "center",
  },
  noResultText: {
    fontFamily: "Roboto-Light",
  },
  sectionTilte: {
    color: color.palette.blue_oec,
    fontFamily: "Roboto-Light",
    fontSize: 18,
    marginBottom: 10,
  },
  welcomeContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  welcomeText: {
    fontFamily: "Roboto-Light",
    fontSize: 18,
  },
})
