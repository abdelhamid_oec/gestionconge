/* eslint-disable @typescript-eslint/no-unused-vars */
import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import { View, Text, ActivityIndicator, ScrollView, Alert, Dimensions } from "react-native"
import { styles } from "./home.style"
import { translate } from "../../i18n"
import { Api } from "../../services/api"
import { useUserStore } from "../../models/user-store/user-store"
import { observer } from "mobx-react-lite"
import { useSoldeStore } from "../../models/solde-store/solde-store"
import moment from "moment"
import { color } from "../../theme"
import Toast from "react-native-toast-message"
import { TouchableOpacity } from "react-native-gesture-handler"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { faFilter } from "@fortawesome/free-solid-svg-icons"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import Modal from "react-native-modal"
import MonthSelectorCalendar from "react-native-month-selector"

const HomeScreen: FC = observer(() => {
  const navigation = useNavigation()
  const userStore = useUserStore()
  const soldeStore = useSoldeStore()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [filterDate, setFilterDate] = useState<any>(moment())
  const [filterDateModal, setFilterDateModal] = useState<boolean>(false)

  const api = new Api()

  const logoutWhenBack = () => {
    Alert.alert("Open Eyes Consulting", "Would you like to signout?", [
      {
        text: "Cancel",
        style: "cancel",
        onPress: () => {
          return true
        },
      },
      {
        text: "Logout",
        style: "destructive",
        // If the user confirmed, then we dispatch the action we blocked earlier
        // This will continue the action that had triggered the removal of the screen
        onPress: () => {
          const isLoggedOut = userStore.logout()
          if (isLoggedOut) {
            navigation.dispatch(() => {
              return navigation.navigate("login")
            })

            Toast.show({
              text1: "Open Eyes Consulting",
              text2: "You Have Been Successfully Signed Out!",
              type: "success",
            })
            console.log("Here")
          } else {
            console.log("Error While Signing out")
          }
        },
      },
    ])
  }

  const onValueChange = (event, newDate) => {
    setFilterDate(newDate)
    setFilterDateModal(false)
    console.log("Date:", moment(filterDate).format("MM-YYYY"))
  }

  const getNotifications = () => {
    userStore
      .getNotifications(api, userStore.user.id_user)
      .then((result) => {
        if (result.length !== 0) {
          userStore.setNotifications(result)
          const unreadNotifications = result.filter((elm) => elm.status == 0)
          if (unreadNotifications.length !== 0) {
            userStore.setUnreadNotifcation(unreadNotifications.length)
          } else {
            userStore.setUnreadNotifcation(null)
          }
        } else {
          userStore.setNotifications([])
          userStore.setUnreadNotifcation(null)
        }
      })
      .catch((err) => {
        console.log("Error While Retreiving Notifications:", err)
      })
  }

  useEffect(() => {
    setIsLoading(true)
    const month = moment().format("M")
    const year = moment().format("YYYY")
    console.log("Month:", month)
    console.log("Year:", year)
    soldeStore
      .getSoldeById(api, userStore.user.id_user, month, year)
      .then((response) => {
        console.log("response:", response)
        soldeStore.setSolde(response.data)
        setIsLoading(false)
      })
      .catch((err) => {
        console.log("Error from Home FrontEnd:", err)
      })
  }, [])

  useEffect(() => {
    getNotifications()
    const notificationInterval = setInterval(() => {
      getNotifications()
    }, 5000)
  }, [])

  return (
    <View style={styles.mainContainer}>
      {isLoading ? (
        <View style={styles.loadingStyle}>
          <ActivityIndicator size="large" color={color.palette.blue_oec} />
        </View>
      ) : (
        <ScrollView style={{ flex: 1 }}>
          <Modal isVisible={filterDateModal}>
            <View style={{ height: "auto" }}>
              <MonthSelectorCalendar
                selectedDate={filterDate}
                onMonthTapped={(date: moment.Moment) => {
                  setFilterDate(date)
                  setFilterDateModal(false)
                  setIsLoading(true)
                  const month = date.format("MM")
                  const year = date.format("YYYY")
                  soldeStore
                    .getSoldeById(api, userStore.user.id_user, month, year)
                    .then((response) => {
                      console.log("response:", response)
                      soldeStore.setSolde(response.data)
                      setIsLoading(false)
                    })
                    .catch((err) => {
                      console.log("Error from Home FrontEnd:", err)
                    })
                }}
              />
            </View>
          </Modal>

          <View style={styles.welcomeContainer}>
            <Text
              onPress={() => {
                console.log("Done")
              }}
              style={styles.welcomeText}
            >
              {translate("homeScreen.welcome")} {userStore.user.name}
            </Text>
            <TouchableOpacity
              style={styles.filterIcon}
              onPress={() => {
                setFilterDateModal(true)
              }}
            >
              <FontAwesomeIcon icon={faFilter} color={color.palette.blue_oec} size={25} />
            </TouchableOpacity>
          </View>
          {soldeStore.solde.length === 0 ? (
            <View style={styles.noResult}>
              <Text style={styles.noResultText}>No Details Available for this account</Text>
            </View>
          ) : (
            <View>
              <View style={styles.congeContainer}>
                <Text style={styles.sectionTilte}>{translate("typeConge.detailConge")}</Text>
                <View style={styles.congeHeader}>
                  <Text style={styles.headerText}>
                    {moment()
                      .month(soldeStore.solde.month - 1)
                      .format("MMMM")}
                    , {soldeStore.solde.year}
                  </Text>
                  <Text style={styles.headerText}>{translate("typeConge.joursRestant")}</Text>
                </View>
                <View style={styles.congeContent}>
                  <View style={styles.conge}>
                    <Text style={styles.congeTitle}>{translate("typeConge.oldSolde")}</Text>
                    <Text style={styles.congeDays}>{soldeStore.solde.old_solde}</Text>
                  </View>
                  <View style={styles.conge}>
                    <Text style={styles.congeTitle}>{translate("typeConge.takenDays")}</Text>
                    <Text style={styles.congeDays}>{soldeStore.solde.taken_days}</Text>
                  </View>
                  <View style={styles.conge}>
                    <Text style={styles.congeTitle}>{translate("typeConge.illnessDays")}</Text>
                    <Text style={styles.congeDays}>{soldeStore.solde.illness_day}</Text>
                  </View>
                  <View style={styles.conge}>
                    <Text style={styles.congeTitle}>{translate("typeConge.currentSolde")}</Text>
                    <Text style={styles.congeDays}>
                      {moment().month() == soldeStore.solde.month - 1
                        ? "-"
                        : parseFloat(soldeStore.solde.old_solde) -
                          (parseFloat(soldeStore.solde.taken_days) +
                            parseFloat(soldeStore.solde.illness_day))}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          )}
        </ScrollView>
      )}
    </View>
  )
})

export default HomeScreen
