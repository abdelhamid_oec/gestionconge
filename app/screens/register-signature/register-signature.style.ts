/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width

export const styles = StyleSheet.create({
  buttonText: {
    color: color.palette.white,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  buttonWrapper: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    height: 50,
    justifyContent: "center",
    marginVertical: 5,
    width: screenWidth - 60,
  },
  image: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  sginaturebuttonStyle: {
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: color.palette.green_oec,
    borderRadius: 20,
    height: 60,
    justifyContent: "center",
    marginTop: 10,
    width: "100%",
  },
  signature: {
    borderColor: "rgba(0,0,0,0.1)",
    flex: 1,
    overflow: "hidden",
  },
  signatureModal: {
    height: Dimensions.get("screen").height / 1.5,
    padding: 25,
    width: screenWidth - 20,
  },
  signatureShowButton: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    flex: 1,
    justifyContent: "center",
    width: 150,
  },
  signatureStyle: {
    alignItems: "center",
    borderRadius: 25,
    flexDirection: "row",
    height: 50,
    justifyContent: "space-between",
    marginBottom: 20,
    marginTop: 10,
    width: Dimensions.get("screen").width - 50,
  },
  signatureText: {
    color: color.palette.white,
    fontFamily: "Roboto-Light",
  },
  signatureTitle: {
    fontFamily: "Roboto-Light",
    fontSize: 14,
    marginVertical: 15,
  },

  textButton: {
    color: color.palette.white,
    fontFamily: "Roboto-Light",
    fontSize: 20,
  },
  title: {
    color: color.palette.black,
    fontFamily: "Roboto-Light",
    marginBottom: 10,
  },
  titleContainer: {
    alignContent: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
})

export const style = `.m-signature-pad {height: 80vh;width:85vh;} .m-signature-pad--footer {
    position: absolute;
    left: 0;
    right: 0;
    bottom: 20px;
    height: 40px;
    
  }`
