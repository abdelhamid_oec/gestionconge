/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { FC, useRef, useState } from "react"
import { ImageBackground, Text, View } from "react-native"
import { styles, style } from "./register-signature.style"
import SignatureScreen, { SignatureViewRef } from "react-native-signature-canvas"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import { useUserStore } from "../../models/user-store/user-store"
import { useNavigation } from "@react-navigation/core"
import { translate } from "../../i18n"

const image = require("../../../assets/images/bg.jpeg")

const RegisterSignatureScreen: FC = (props) => {
  const [signature, setSignature] = useState<string>("")

  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const userStore = useUserStore()
  const navigation = useNavigation()
  const handleRegister = (registerData) => {
    const api = new Api()
    userStore.addUser(api, registerData).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Vacation Leave Request",
          text2: translate("Toast.signUp"),
          topOffset: 30,
        })
        navigation.navigate("login")
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "add user error",
          text2: response.data,
          topOffset: 30,
        })

        setIsAddLoading(false)
      }
    })
  }
  const ref = useRef<SignatureViewRef>()

  const handleOK = (signature) => {
    const registerData = props.route.params.data

    setSignature(signature)
    registerData.signature = signature
    handleRegister(registerData)
  }

  const handleEmpty = () => {
    console.log("Empty")
    console.log("Signature:", signature)
  }

  const handleClear = () => {
    setSignature("")
    console.log("clear success!")
  }

  const handleEnd = () => {
    console.log("End")
  }

  return (
    <ImageBackground source={image} resizeMode="cover" blurRadius={15} style={styles.image}>
      <View style={styles.signatureModal}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Signature</Text>
        </View>
        <View style={styles.signature}>
          <SignatureScreen
            ref={ref}
            onEnd={handleEnd}
            onOK={handleOK}
            onEmpty={handleEmpty}
            onClear={handleClear}
            autoClear={false}
            descriptionText={"Signature"}
            webStyle={style}
          />
        </View>
      </View>
    </ImageBackground>
  )
}

export default RegisterSignatureScreen
