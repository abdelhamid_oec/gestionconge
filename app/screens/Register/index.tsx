/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState } from "react"
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  ImageBackground,
  ScrollView,
  Alert,
} from "react-native"
import { Style } from "./register.style"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faUser,
  faLock,
  faEnvelope,
  faIdCard,
  faHome,
  faPhone,
  faCalendar,
} from "@fortawesome/free-solid-svg-icons"
import { translate } from "../../i18n"
import { useNavigation } from "@react-navigation/core"
import { color } from "../../theme"
import {
  validateEmail,
  validatePassword,
  validatePhone,
  validateName,
  validateAddress,
} from "../../utils/validation"
import DateTimePickerModal from "react-native-modal-datetime-picker"

const image = require("../../../assets/images/bg.jpeg")
export type RegisterData = {
  name: string
  surname: string
  identity: string
  personnalEmail: string
  address: string
  phoneNumber: string
  optionnalPhoneNumber: string
  email: string
  password: string
  startWorkingDate: Date
  signature: string
  type: string
}
const RegisterScreen: FC = () => {
  const [emailFoccus, setEmailFoccus] = useState<boolean>(false)
  const [passwordFoccus, setpasswordFoccus] = useState<boolean>(false)
  const [nameFoccus, setNameFoccus] = useState<boolean>(false)
  const [surnameFoccus, setSurnameFoccus] = useState<boolean>(false)
  const [addressFoccus, setAddressFoccus] = useState<boolean>(false)
  const [phoneFoccus, setPhoneFoccus] = useState<boolean>(false)
  const [cinFoccus, setCinFoccus] = useState<boolean>(false)
  const [personnalemail, setPersonnalEmailFoccus] = useState<boolean>(false)
  const [phone2Foccus, setPhone2Foccus] = useState<boolean>(false)
  const [startDateFoccus, setStartDateFoccus] = useState<boolean>(false)
  const [isDateOpened, setIsDateOpened] = useState<boolean>(false)

  const [workingDate, setWorkingDate] = useState<Date>(new Date())
  const [workingDateText, setworkingDateText] = useState<string>("")
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [personnalEmail, setpersonnalEmail] = useState<string>("")
  const [surname, setSurname] = useState<string>("")
  const [name, setName] = useState<string>("")
  const [cin, setCin] = useState<string>("")
  const [phone, setPhone] = useState<string>("")
  const [address, setAddress] = useState<string>("")
  const [numphone2, setNumTel2] = useState<string>("")

  const navigation = useNavigation()

  const handleRegister = () => {
    const RegisterTypeData: RegisterData = {
      name: name,
      surname: surname,
      identity: cin,
      personnalEmail: personnalEmail,
      address: address,
      phoneNumber: phone,
      optionnalPhoneNumber: numphone2,
      email: email,
      password: password,
      startWorkingDate: workingDate,
      signature: "",
      type: "user",
    }
    const passValidation = validatePassword(RegisterTypeData.password)
    const nameValidation = validateName(RegisterTypeData.name)
    const surnameValidation = validateName(RegisterTypeData.surname)
    const identityValidation = validatePhone(RegisterTypeData.identity)
    const addressValidation = validateAddress(RegisterTypeData.address)
    const phoneValidation = validatePhone(RegisterTypeData.phoneNumber)

    if (nameValidation.error === true) {
      Alert.alert("Register", nameValidation.msg)
    } else if (surnameValidation.error === true) {
      Alert.alert("Register", surnameValidation.msg)
    } else if (identityValidation.error === true) {
      Alert.alert("Register", identityValidation.msg)
    } else if (validateEmail(RegisterTypeData.personnalEmail) === false) {
      Alert.alert("Register", "Please enter a valid email")
    } else if (addressValidation.error === true) {
      Alert.alert("Register", addressValidation.msg)
    } else if (phoneValidation.error === true) {
      Alert.alert("Register", phoneValidation.msg)
    } else if (validateEmail(RegisterTypeData.email) === false) {
      Alert.alert("Register", "Please enter a valid email")
    } else if (passValidation.error === true) {
      Alert.alert("Register", passValidation.msg)
    } else {
      navigation.navigate("registerSignature", { data: RegisterTypeData })
    }
  }
  return (
    <ImageBackground source={image} resizeMode="cover" blurRadius={15} style={Style.image}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ alignItems: "center" }}
      >
        <View style={Style.textWrapper}>
          <Text style={Style.welcomText}>
            {translate("registerScreen.welcom")}
            {"\n"}
            {translate("registerScreen.oec")}
          </Text>
        </View>

        <View style={nameFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={nameFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faUser}
              size={20}
              color={nameFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setName(text)
            }}
            value={name}
            placeholder={translate("registerScreen.name")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setNameFoccus(true)
            }}
            onBlur={() => {
              setNameFoccus(false)
            }}
          />
        </View>

        <View style={surnameFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={surnameFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faUser}
              size={20}
              color={surnameFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setSurname(text)
            }}
            value={surname}
            placeholder={translate("registerScreen.surname")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setSurnameFoccus(true)
            }}
            onBlur={() => {
              setSurnameFoccus(false)
            }}
          />
        </View>

        <View style={cinFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={cinFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faIdCard}
              size={20}
              color={cinFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setCin(text)
            }}
            keyboardType={"number-pad"}
            value={cin}
            placeholder={translate("registerScreen.cin")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setCinFoccus(true)
            }}
            onBlur={() => {
              setCinFoccus(false)
            }}
          />
        </View>

        <View style={personnalemail ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={personnalemail ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faEnvelope}
              size={20}
              color={personnalemail ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setpersonnalEmail(text)
            }}
            value={personnalEmail}
            keyboardType={"email-address"}
            placeholder={translate("registerScreen.emailPersonel")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setPersonnalEmailFoccus(true)
            }}
            onBlur={() => {
              setPersonnalEmailFoccus(false)
            }}
          />
        </View>

        <View style={addressFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={addressFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faHome}
              size={20}
              color={addressFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setAddress(text)
            }}
            value={address}
            placeholder={translate("registerScreen.address")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setAddressFoccus(true)
            }}
            onBlur={() => {
              setAddressFoccus(false)
            }}
          />
        </View>

        <View style={phoneFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={phoneFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faPhone}
              size={20}
              color={phoneFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setPhone(text)
            }}
            value={phone}
            keyboardType={"phone-pad"}
            placeholder={translate("registerScreen.phone")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setPhoneFoccus(true)
            }}
            onBlur={() => {
              setPhoneFoccus(false)
            }}
          />
        </View>

        <View style={phone2Foccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={phone2Foccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faPhone}
              size={20}
              color={phone2Foccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setNumTel2(text)
            }}
            value={numphone2}
            keyboardType={"phone-pad"}
            placeholder={translate("registerScreen.phone2")}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setPhone2Foccus(true)
            }}
            onBlur={() => {
              setPhone2Foccus(false)
            }}
          />
        </View>

        <DateTimePickerModal
          mode="date"
          isVisible={isDateOpened}
          onConfirm={(workingDate) => {
            setIsDateOpened(false)
            setWorkingDate(workingDate)
            setworkingDateText(workingDate.toDateString())
          }}
          onCancel={() => {
            setIsDateOpened(false)
          }}
        />
        <TouchableOpacity
          onPress={() => {
            setIsDateOpened(true)
          }}
          style={startDateFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}
        >
          <View style={startDateFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faCalendar}
              size={20}
              color={startDateFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>
          <View style={Style.input}>
            <TextInput
              placeholder={translate("typeConge.dateDebut")}
              placeholderTextColor={color.palette.white}
              editable={false}
              value={workingDateText}
            />
          </View>
        </TouchableOpacity>

        <View style={emailFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={emailFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faEnvelope}
              size={20}
              color={emailFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setEmail(text)
            }}
            value={email}
            placeholder={translate("registerScreen.email")}
            keyboardType={"email-address"}
            placeholderTextColor={color.palette.white}
            onFocus={() => {
              setEmailFoccus(true)
            }}
            onBlur={() => {
              setEmailFoccus(false)
            }}
          />
        </View>

        <View style={passwordFoccus ? Style.inputWrapperFoccused : Style.inputWrapper}>
          <View style={passwordFoccus ? Style.iconWrapperFoccused : Style.iconWrapper}>
            <FontAwesomeIcon
              icon={faLock}
              size={20}
              color={passwordFoccus ? color.palette.blue_oec : color.palette.white}
            />
          </View>

          <TextInput
            style={Style.input}
            onChangeText={(text) => {
              setPassword(text)
            }}
            value={password}
            keyboardType={"number-pad"}
            placeholder={translate("registerScreen.password")}
            placeholderTextColor={color.palette.white}
            secureTextEntry={true}
            onFocus={() => {
              setpasswordFoccus(true)
            }}
            onBlur={() => {
              setpasswordFoccus(false)
            }}
          />
        </View>

        <TouchableOpacity
          style={Style.buttonWrapper}
          onPress={() => {
            handleRegister()
          }}
        >
          <Text style={Style.textButton}>{translate("registerScreen.next")}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={Style.buttonWrapperLogIn}
          onPress={() => {
            navigation.navigate("login")
          }}
        >
          <Text style={Style.textButton}>{translate("logInScreen.signIn")}</Text>
        </TouchableOpacity>
      </ScrollView>
    </ImageBackground>
  )
}

export default RegisterScreen
