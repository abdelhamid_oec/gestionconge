/* eslint-disable react-native/no-color-literals */
import { StyleSheet, Dimensions } from "react-native"
import { color } from "../../theme"

const screenWidth = Dimensions.get("screen").width
export const Style = StyleSheet.create({
  buttonWrapper: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    height: 50,
    justifyContent: "center",
    marginVertical: 5,
    width: screenWidth - 60,
  },
  buttonWrapperLogIn: {
    alignItems: "center",
    backgroundColor: color.palette.green_oec,
    borderRadius: 20,
    height: 50,
    justifyContent: "center",
    marginBottom: 10,
    marginVertical: 5,
    width: screenWidth - 60,
  },
  iconWrapper: {
    alignItems: "center",
    borderRightColor: color.palette.white,
    borderRightWidth: 1,
    height: 40,
    justifyContent: "center",
    width: 50,
  },
  iconWrapperFoccused: {
    alignItems: "center",
    borderRightColor: color.palette.blue_oec,
    borderRightWidth: 1,
    height: 40,
    justifyContent: "center",
    width: 50,
  },
  image: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  input: {
    color: color.palette.white,
    flex: 1,
    fontFamily: "Roboto-Light",
    height: 50,
    marginLeft: 5,
    overflow: "hidden",
    paddingHorizontal: 10,
  },
  inputWrapper: {
    alignItems: "center",
    borderColor: color.palette.white,
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: "row",
    height: 50,
    marginVertical: 5,
    width: screenWidth - 60,
  },
  inputWrapperFoccused: {
    alignItems: "center",
    borderColor: color.palette.blue_oec,
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: "row",
    height: 50,
    marginVertical: 5,
    width: screenWidth - 60,
  },
  logoWrapper: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
    width: 150,
  },
  switchWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  textButton: {
    color: color.palette.white,
    fontFamily: "Roboto-Light",
    fontSize: 20,
  },
  textSwitch: {
    color: color.palette.white,
    fontFamily: "Roboto-Light",
    fontSize: 15,
  },
  textWrapper: {
    alignContent: "center",
    justifyContent: "center",
    marginVertical: 15,
  },
  welcomText: {
    color: color.palette.blue_oec,
    fontFamily: "Roboto-Light",
    fontSize: 25,
    textAlign: "center",
  },
})
