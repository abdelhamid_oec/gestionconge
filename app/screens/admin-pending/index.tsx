/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React, { FC, useState, useEffect } from "react"
import { FlatList, View, Text, Alert, Image } from "react-native"
import { styles } from "./admin-pending.style"
import PendingRequestAdmin, { DataProps } from "../../components/pending-request-admin"
import BottomSheet from "reanimated-bottom-sheet"
import { useUserStore } from "../../models/user-store/user-store"
import { Api } from "../../services/api"
import { useCongeStore } from "../../models/conge-store/conge-store"
import Toast from "react-native-toast-message"
import { observer } from "mobx-react-lite"
import { translate } from "../../i18n"
import Dialog from "react-native-dialog"

const AdminPendingScreen: FC = observer(() => {
  type UserDataType = {
    name: string
    surname: string
    email: string
    userSignature: string
    congeSignature: string
  }

  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const [visible, setVisible] = useState(false)
  const [currentrequest, setCurrentRequest] = useState<any>({
    idConge: "",
  })
  const [reason, setReason] = useState<string>()
  const [userData, setUserData] = useState<UserDataType>({
    name: "",
    surname: "",
    email: "",
    userSignature: "",
    congeSignature: "",
  })
  const congeStore = useCongeStore()
  const userStore = useUserStore()
  const api = new Api()

  useEffect(() => {
    getNotifications()
    const notificationInterval = setInterval(() => {
      getNotifications()
    }, 5000)
  }, [])

  useEffect(() => {
    handlePendingRequest()
    const interval = setInterval(() => {
      handlePendingRequest()
    }, 5000)
    return () => {
      clearInterval(interval)
    }
  }, [])

  const showDialog = () => {
    setVisible(true)
  }

  const handleCancel = () => {
    setVisible(false)
  }

  const getNotifications = () => {
    userStore
      .getNotifications(api, userStore.user.id_user)
      .then((result) => {
        if (result.length !== 0) {
          userStore.setNotifications(result)
          const unreadNotifications = result.filter((elm) => elm.status == 0)
          if (unreadNotifications.length !== 0) {
            userStore.setUnreadNotifcation(unreadNotifications.length)
          } else {
            userStore.setUnreadNotifcation(null)
          }
        } else {
          userStore.setNotifications([])
          userStore.setUnreadNotifcation(null)
        }
      })
      .catch((err) => {
        console.log("Error While Retreiving Notifications:", err)
      })
  }

  const AcceptButtonAlert = (idConge) =>
    Alert.alert("Add Congé", translate("Toast.addConge"), [
      {
        text: "Cancel",
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleAccept(idConge)
          handlePendingRequest()
        },
      },
    ])

  const handlePendingRequest = () => {
    setIsAddLoading(true)
    const api = new Api()

    congeStore.getPendingRequest(api).then((response) => {
      if (response.status === 200) {
        congeStore.setPendingRequest(response.data)

        setIsAddLoading(false)
      } else {
        console.log("Pending Request is not working")

        setIsAddLoading(false)
      }
    })
  }

  const handleAccept = (idConge) => {
    setIsAddLoading(true)
    const api = new Api()

    congeStore.acceptConge(api, idConge).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
      }
    })
  }

  const handleDelete = (idConge, reason) => {
    setIsAddLoading(true)
    const api = new Api()

    congeStore.deleteConge(api, idConge, reason).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
        setReason("")
      } else {
        Toast.show({
          type: "error",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
        setReason("")
      }
    })
  }

  const renderContent = () => (
    <View style={styles.bottomSheetContent}>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.name")} : </Text>
        <Text style={styles.TextStyle}>{userData.name || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.surname")} : </Text>
        <Text style={styles.TextStyle}>{userData.surname || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.ProffessionalEmail")} : </Text>
        <Text style={styles.TextStyle}> {userData.email || ""}</Text>
      </View>
      <View style={styles.imageWrapper}>
        <View>
          <Image
            source={{ uri: "http://rh.openeyesconsulting.com" + userData.congeSignature }}
            style={{ height: 150, width: 150 }}
          ></Image>
          <Text>{translate("pendingHolidayRequest.holidaySignature")}</Text>
        </View>
        <View>
          <Image
            source={{ uri: "http://rh.openeyesconsulting.com" + userData.userSignature }}
            style={{ height: 150, width: 150 }}
          ></Image>
          <Text>{translate("pendingHolidayRequest.userSignature")}</Text>
        </View>
      </View>
    </View>
  )

  const renderHeader = () => (
    <View style={styles.bottomSheetHeader}>
      <View style={styles.headerView} />
    </View>
  )

  const sheetRef = React.useRef(null)

  return (
    <View style={styles.mainContainer}>
      <View>
        <Dialog.Container visible={visible}>
          <Dialog.Title>{translate("pendingHolidayRequest.deleteHolidayRequest")}</Dialog.Title>
          <Dialog.Input
            multiline={true}
            value={reason}
            onChangeText={(text) => {
              setReason(text)
            }}
          ></Dialog.Input>
          <Dialog.Description>{translate("Toast.deleteConge")}</Dialog.Description>
          <Dialog.Button label="Cancel" onPress={handleCancel} />
          <Dialog.Button
            label="Delete"
            onPress={() => {
              handleDelete(currentrequest, reason)
              setVisible(false)
            }}
          />
        </Dialog.Container>
      </View>
      <BottomSheet
        ref={sheetRef}
        initialSnap={1}
        snapPoints={[450, 0]}
        borderRadius={10}
        renderContent={renderContent}
        renderHeader={renderHeader}
      />
      {congeStore.pendingRequest.length === 0 ? (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>{translate("common.noHolidayAvailable")}</Text>
        </View>
      ) : (
        <FlatList
          numColumns={1}
          data={congeStore.pendingRequest}
          contentContainerStyle={{ paddingBottom: 10 }}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            const request: DataProps = {
              nom: item.name,
              prenom: item.surname,
              type: item.holiday_type,
              dateDebut: item.start_date,
              dateFin: item.end_date,
            }
            return (
              <PendingRequestAdmin
                onCheck={() => {
                  AcceptButtonAlert(item.id_conge)
                }}
                onDelete={() => {
                  showDialog()
                  setCurrentRequest(item.id_conge)
                }}
                onDetail={() => {
                  setUserData({
                    name: item.name,
                    surname: item.surname,
                    email: item.email,
                    userSignature: item.userSignature,
                    congeSignature: item.congeSignature,
                  })
                  sheetRef.current.snapTo(0)
                }}
                data={request}
              />
            )
          }}
        />
      )}
    </View>
  )
})

export default AdminPendingScreen
