/* eslint-disable no-dupe-keys */
import { StyleSheet } from "react-native"
import { color } from "../../theme"

export const styles = StyleSheet.create({
  emptyContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  emptyText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
  },
  formContainer: {
    alignItems: "center",
  },
  mainContainer: {
    flex: 1,
  },
})
