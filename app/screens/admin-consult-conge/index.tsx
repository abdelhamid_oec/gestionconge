/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState, useEffect } from "react"
import { View, FlatList, Alert, Text } from "react-native"
import { styles } from "./admin-consult-conge.style"
import ConsultCongeadmin, { DataProps } from "../../components/consult-conge-admin"
import { usecommonHolidayStore } from "../../models/common-holiday-store/common-holiday-store"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import { observer } from "mobx-react-lite"
import { translate } from "../../i18n"
import { useNavigation } from "@react-navigation/core"
const AdminConsultCongeScreen: FC = observer(() => {
  type CommonHolidayType = {
    id: string
    name: string
    startDate: string
    endDate: string
    description: string
    type: string
  }
  const [commonHolidayData, setCommonHolidayData] = useState<CommonHolidayType>({
    id: "",
    name: "",
    startDate: "",
    endDate: "",
    description: "",
    type: "",
  })
  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)

  const navigation = useNavigation()
  const commonHolidayStore = usecommonHolidayStore()

  useEffect(() => {
    handleCommonHoliday()
    const interval = setInterval(() => {
      handleCommonHoliday()
    }, 5000)
    return () => {
      clearInterval(interval)
    }
  }, [])

  const DeleteButtonAlert = (idHoliday) =>
    Alert.alert("Alert Title", "Delete Msg", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleDelete(idHoliday)
          handleCommonHoliday()
        },
      },
    ])

  const handleCommonHoliday = () => {
    setIsAddLoading(true)
    const api = new Api()

    commonHolidayStore.getAllCommonHoliday(api).then((response) => {
      if (response.status === 200) {
        commonHolidayStore.setCommonHolidayData(response.data)

        setIsAddLoading(false)
      } else {
        console.log("Common Holiday is not working")

        setIsAddLoading(false)
      }
    })
  }

  const handleDelete = (idHoliday) => {
    setIsAddLoading(true)
    const api = new Api()

    commonHolidayStore.deleteCommonHoliday(api, idHoliday).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
      }
    })
  }

  return (
    <View style={styles.mainContainer}>
      {commonHolidayStore.commonHolidayData.length === 0 ? (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>{translate("common.noHolidayAvailable")}</Text>
        </View>
      ) : (
        <FlatList
          data={commonHolidayStore.commonHolidayData}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{ paddingBottom: 10 }}
          renderItem={({ item }) => {
            const request: DataProps = {
              nomHoliday: item.name,
              dateDebut: item.start_date,
              dateFin: item.end_date,
            }
            return (
              <ConsultCongeadmin
                onEdit={() => {
                  setCommonHolidayData({
                    id: item.id_holiday,
                    name: item.name,
                    startDate: item.start_date,
                    endDate: item.end_date,
                    description: item.description,
                    type: item.type,
                  })
                  navigation.navigate("editCommonHoliday", {
                    data: {
                      id: item.id_holiday,
                      name: item.name,
                      startDate: item.start_date,
                      endDate: item.end_date,
                      description: item.description,
                      type: item.type,
                    },
                  })
                }}
                onDelete={() => {
                  DeleteButtonAlert(item.id_holiday)
                }}
                data={request}
              />
            )
          }}
        />
      )}
    </View>
  )
})

export default AdminConsultCongeScreen
