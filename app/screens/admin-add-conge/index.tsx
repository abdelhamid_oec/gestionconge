/* eslint-disable react/jsx-no-undef */
/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { ScrollView, TextInput, View, TouchableOpacity, Text } from "react-native"
import { color } from "../../theme"
import { styles } from "./admin-add-conge.style"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import { faCalendar } from "@fortawesome/free-solid-svg-icons"
import { translate } from "../../i18n"
import RNPickerSelect from "react-native-picker-select"
import { usecommonHolidayStore } from "../../models/common-holiday-store/common-holiday-store"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import { CommonHolidayTypeData } from "../../services/api/common-holiday-api"

const AdminAddCongeScreen: FC = () => {
  const [startDate, setStartDate] = useState<Date>(new Date())
  const [endDate, setEndDate] = useState<Date>(new Date())
  const [startDateText, setStartDateText] = useState<string>("")
  const [endDateText, setEndDateText] = useState<string>("")
  const [name, setName] = useState<string>("")
  const [typeConge, setTypeConge] = useState<string>("")
  const [description, setDescription] = useState<string>("")
  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const [isDateOpened, setIsDateOpened] = useState<boolean>(false)
  const [isEndDateOpened, setIsEndDateOpened] = useState<boolean>(false)

  const commonHolidayStore = usecommonHolidayStore()

  const handleAdd = () => {
    setIsAddLoading(true)
    const api = new Api()
    const data: CommonHolidayTypeData = {
      name: name,
      startDate: startDate,
      endDate: endDate,
      type: typeConge,
      description: description,
    }
    commonHolidayStore.addCommonHoliday(api, data).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
        setName("")
        setStartDateText("")
        setEndDateText("")
        setTypeConge("")
        setDescription("")
      } else {
        Toast.show({
          type: "error",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
        setName("")
        setStartDateText("")
        setEndDateText("")
        setTypeConge("")
        setDescription("")
      }
    })
  }

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{ alignItems: "center", paddingBottom: 10 }}
    >
      <View style={styles.formContainer}>
        <DateTimePickerModal
          mode="date"
          isVisible={isDateOpened}
          onConfirm={(startDate) => {
            setIsDateOpened(false)
            setStartDate(startDate)
            setStartDateText(startDate.toDateString())
          }}
          onCancel={() => {
            setIsDateOpened(false)
          }}
        />

        <DateTimePickerModal
          mode="date"
          isVisible={isEndDateOpened}
          onConfirm={(date) => {
            setIsEndDateOpened(false)
            setEndDate(date)
            setEndDateText(date.toDateString())
          }}
          onCancel={() => {
            setIsEndDateOpened(false)
          }}
        />
        <View style={styles.dateHolder}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => {
              setName(text)
            }}
            value={name}
            placeholder={translate("typeConge.holidayName")}
            placeholderTextColor={color.palette.lightGrey}
          />
        </View>
        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateDebut")}
            placeholderTextColor={color.palette.lightGrey}
            editable={false}
            value={startDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        </View>

        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateFin")}
            editable={false}
            value={endDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsEndDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        </View>
        <View style={[styles.formInput, { justifyContent: "center" }]}>
          <RNPickerSelect
            value={typeConge}
            style={{
              inputAndroid: { color: "black" },
            }}
            onValueChange={(value) => {
              setTypeConge(value)
            }}
            items={[
              { label: "Paye", value: "paye" },
              { label: "Non paye", value: "nonPaye" },
            ]}
          ></RNPickerSelect>
        </View>

        <View style={styles.dateHolder}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => {
              setDescription(text)
            }}
            value={description}
            placeholder={translate("typeConge.description")}
            placeholderTextColor={color.palette.lightGrey}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            handleAdd()
          }}
          style={styles.buttonStyle}
        >
          <Text style={styles.buttonText}>{translate("congeScreen.addButton")}</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}

export default AdminAddCongeScreen
