/* eslint-disable react-native/no-color-literals */
import { Dimensions, StyleSheet } from "react-native"
import { color } from "../../theme"
const screenWidth = Dimensions.get("screen").width
export const styles = StyleSheet.create({
  buttonStyle: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    height: 60,
    justifyContent: "center",
    marginTop: 10,
    width: screenWidth - 60,
  },
  buttonText: {
    color: color.palette.white,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  dateHolder: {
    alignItems: "center",
    borderColor: color.palette.lightGrey,
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: "row",
    fontFamily: "Roboto-Light",
    height: 60,
    justifyContent: "space-between",
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  formContainer: {
    alignItems: "center",
    paddingTop: 30,
  },

  formInput: {
    borderColor: color.palette.lightGrey,
    borderRadius: 20,
    borderWidth: 1,
    fontFamily: "Roboto-Light",
    height: 60,
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  input: {
    color: color.palette.lightGrey,
    flex: 1,
    fontFamily: "Roboto-Light",
    height: 50,
    marginLeft: 5,
    overflow: "hidden",
    paddingHorizontal: 10,
  },
})
