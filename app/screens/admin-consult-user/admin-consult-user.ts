/* eslint-disable react-native/no-color-literals */
import { StyleSheet } from "react-native"
import { color } from "../../theme"

export const styles = StyleSheet.create({
  TextStyle: {
    fontFamily: "Roboto-Light",
    fontSize: 18,
  },
  TextStyleLabel: {
    color: color.palette.dark_grey,
    fontFamily: "Roboto-Light",
    fontSize: 18,
  },
  bottomSheetContent: {
    backgroundColor: color.palette.white,
    flexWrap: "wrap",
    height: 450,
    marginHorizontal: 5,
  },
  bottomSheetHeader: {
    alignItems: "center",
    backgroundColor: color.palette.white,
    borderBottomColor: color.palette.lightGrey,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    height: 50,
    justifyContent: "center",
    marginHorizontal: 5,
    padding: 16,
  },
  emptyContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  emptyText: {
    color: color.palette.lightGrey,
    fontFamily: "Roboto-Light",
  },
  headerView: {
    borderColor: color.palette.lightGrey,
    borderRadius: 20,
    borderWidth: 2,
    width: 50,
  },
  mainContainer: {
    flex: 1,
  },
  textWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 10,
    marginLeft: 10,
  },
})
