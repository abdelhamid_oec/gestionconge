/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
import React, { FC, useEffect, useState } from "react"
import { View, FlatList, Text, Alert } from "react-native"
import UserCard, { DataProps } from "../../components/user-card"
import { useUserStore } from "../../models/user-store/user-store"
import { Api } from "../../services/api"
import { observer } from "mobx-react-lite"
import { styles } from "./admin-consult-user"
import BottomSheet from "reanimated-bottom-sheet"
import Toast from "react-native-toast-message"
import { translate } from "../../i18n"

const ConsultUserScreen: FC = observer(() => {
  type UserDataType = {
    name: string
    surname: string
    email: string
    personalEmail: string
    primaryPhone: string
    secondaryPhone?: string
    address: string
    identity: string
    status: string
  }

  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const userStore = useUserStore()

  const [userData, setUserData] = useState<UserDataType>({
    name: "",
    surname: "",
    email: "",
    personalEmail: "",
    primaryPhone: "",
    secondaryPhone: "",
    address: "",
    identity: "",
    status: "",
  })

  useEffect(() => {
    handleUser()
  }, [])

  const DeactivateButtonAlert = (idUser) =>
    Alert.alert("Alert Title", "edit User Msg", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleDeactivate(idUser)
          handleUser()
        },
      },
    ])

  const handleDeactivate = (idUser) => {
    setIsAddLoading(true)
    const api = new Api()

    userStore.deactivateUser(api, idUser).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "deactivate user Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "deactivate user Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
      }
    })
  }

  const handleUser = () => {
    setIsAddLoading(true)
    const api = new Api()

    userStore.getSpecificUsers(api).then((response) => {
      if (response.status === 200) {
        userStore.setUsers(response.data)

        setIsAddLoading(false)
      } else {
        console.log("all users is not working")

        setIsAddLoading(false)
      }
    })
  }

  const renderContent = () => (
    <View style={styles.bottomSheetContent}>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.name")} : </Text>
        <Text style={styles.TextStyle}>{userData.name || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.surname")} : </Text>
        <Text style={styles.TextStyle}>{userData.surname || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.identity")} : </Text>
        <Text style={styles.TextStyle}>{userData.identity || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.personalEmail")} : </Text>
        <Text style={styles.TextStyle}>{userData.personalEmail || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.adress")} : </Text>
        <Text style={styles.TextStyle}>{userData.address || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.primaryPhone")} : </Text>
        <Text style={styles.TextStyle}>{userData.primaryPhone || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.secondaryPhone")} : </Text>
        <Text style={styles.TextStyle}>{userData.secondaryPhone || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.ProffessionalEmail")} : </Text>
        <Text style={styles.TextStyle}> {userData.email || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.status")} : </Text>
        <Text style={styles.TextStyle}> {userData.status || ""}</Text>
      </View>
    </View>
  )

  const renderHeader = () => (
    <View style={styles.bottomSheetHeader}>
      <View style={styles.headerView} />
    </View>
  )

  const sheetRef = React.useRef(null)

  return (
    <View style={styles.mainContainer}>
      <BottomSheet
        ref={sheetRef}
        initialSnap={1}
        snapPoints={[400, 0]}
        borderRadius={10}
        renderContent={renderContent}
        renderHeader={renderHeader}
      />
      {userStore.users.length === 0 ? (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>No User Available</Text>
        </View>
      ) : (
        <FlatList
          data={userStore.users}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{ paddingBottom: 10 }}
          renderItem={({ item }) => {
            const request: DataProps = {
              name: item.name,
              surname: item.surname,
              email: item.email,
              dateDebut: item.starting_work_date,
              status: item.status,
            }
            return (
              <UserCard
                data={request}
                onEdit={() => {
                  DeactivateButtonAlert(item.id_user)
                }}
                onDetail={() => {
                  setUserData({
                    name: item.name,
                    surname: item.surname,
                    email: item.email,
                    personalEmail: item.personal_email,
                    primaryPhone: item.primary_phone,
                    secondaryPhone: item.secondary_phone,
                    address: item.address,
                    identity: item.identity,
                    status: item.status,
                  })
                  sheetRef.current.snapTo(0)
                }}
              />
            )
          }}
        />
      )}
    </View>
  )
})

export default ConsultUserScreen
