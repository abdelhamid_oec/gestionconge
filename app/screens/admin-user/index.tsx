/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState, useEffect } from "react"
import { FlatList, View, Text, Alert } from "react-native"
import { styles } from "./admin-user.style"
import UseradminCard, { DataProps } from "../../components/user-admin-card"
import BottomSheet from "reanimated-bottom-sheet"
import { observer } from "mobx-react-lite"
import { useUserStore } from "../../models/user-store/user-store"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import { translate } from "../../i18n"
const AdminUserScreen: FC = observer(() => {
  type UserDataType = {
    name: string
    surname: string
    email: string
    personalEmail: string
    primaryPhone: string
    secondaryPhone?: string
    address: string
    identity: string
  }

  const userStore = useUserStore()
  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const [userData, setUserData] = useState<UserDataType>({
    name: "",
    surname: "",
    email: "",
    personalEmail: "",
    primaryPhone: "",
    secondaryPhone: "",
    address: "",
    identity: "",
  })

  useEffect(() => {
    handlePendingRequest()
    const interval = setInterval(() => {
      handlePendingRequest()
    }, 5000)
    return () => {
      clearInterval(interval)
    }
  }, [])

  const DeleteButtonAlert = (idUser) =>
    Alert.alert("Delete User", translate("Toast.deleteUser"), [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleDelete(idUser)
          handlePendingRequest()
        },
      },
    ])

  const AcceptButtonAlert = (idUser) =>
    Alert.alert("Alert Title", "Delete User Msg", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleAccept(idUser)
          handlePendingRequest()
        },
      },
    ])

  const handlePendingRequest = () => {
    setIsAddLoading(true)
    const api = new Api()

    userStore.getpendingUsers(api).then((response) => {
      if (response.status === 200) {
        userStore.setPendingUser(response.data)

        setIsAddLoading(false)
      } else {
        console.log("Pending Request is not working")

        setIsAddLoading(false)
      }
    })
  }

  const handleAccept = (idUser) => {
    setIsAddLoading(true)
    const api = new Api()

    userStore.acceptUser(api, idUser).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "accept user Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "accept user Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
      }
    })
  }

  const handleDelete = (idUser) => {
    setIsAddLoading(true)
    const api = new Api()

    userStore.deleteUser(api, idUser).then((response) => {
      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })
        setIsAddLoading(false)
      } else {
        Toast.show({
          type: "error",
          text1: "Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
      }
    })
  }

  const renderContent = () => (
    <View style={styles.bottomSheetContent}>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.name")} : </Text>
        <Text style={styles.TextStyle}>{userData.name || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.surname")} : </Text>
        <Text style={styles.TextStyle}>{userData.surname || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.identity")} : </Text>
        <Text style={styles.TextStyle}>{userData.identity || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.personalEmail")} : </Text>
        <Text style={styles.TextStyle}>{userData.personalEmail || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.adress")} : </Text>
        <Text style={styles.TextStyle}>{userData.address || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.primaryPhone")} : </Text>
        <Text style={styles.TextStyle}>{userData.primaryPhone || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.secondaryPhone")} : </Text>
        <Text style={styles.TextStyle}>{userData.secondaryPhone || ""}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.TextStyleLabel}>{translate("BottomSheet.ProffessionalEmail")} : </Text>
        <Text style={styles.TextStyle}> {userData.email || ""}</Text>
      </View>
    </View>
  )

  const renderHeader = () => (
    <View style={styles.bottomSheetHeader}>
      <View style={styles.headerView} />
    </View>
  )

  const sheetRef = React.useRef(null)

  return (
    <View style={styles.mainContainer}>
      <BottomSheet
        ref={sheetRef}
        initialSnap={1}
        snapPoints={[400, 0]}
        borderRadius={10}
        renderContent={renderContent}
        renderHeader={renderHeader}
      />
      {userStore.pendingUser.length === 0 ? (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText}>{translate("common.noPendingRequest")}</Text>
        </View>
      ) : (
        <FlatList
          numColumns={2}
          data={userStore.pendingUser}
          contentContainerStyle={{ paddingLeft: 7.5 }}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            const request: DataProps = {
              name: item.name,
              surname: item.surname,
            }

            return (
              <UseradminCard
                onCheck={() => {
                  AcceptButtonAlert(item.id_user)
                }}
                onDelete={() => {
                  DeleteButtonAlert(item.id_user)
                }}
                onDetail={() => {
                  setUserData({
                    name: item.name,
                    surname: item.surname,
                    email: item.email,
                    personalEmail: item.personal_email,
                    primaryPhone: item.primary_phone,
                    secondaryPhone: item.secondary_phone,
                    address: item.address,
                    identity: item.identity,
                  })
                  sheetRef.current.snapTo(0)
                }}
                data={request}
              />
            )
          }}
        />
      )}
    </View>
  )
})

export default AdminUserScreen
