import React, { FC } from "react"
import { View, FlatList } from "react-native"
import { styles } from "./suivi-conge.style"
import RequestCard from "../../components/request-card"

const SuiviCongeScreen: FC = () => {
  return (
    <View style={styles.mainContainer}>
      <FlatList
        data={[""]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => <RequestCard />}
      />
    </View>
  )
}

export default SuiviCongeScreen
