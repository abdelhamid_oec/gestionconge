/* eslint-disable react-native/no-color-literals */
import { Dimensions, StyleSheet } from "react-native"
import { color } from "../../theme"
const screenWidth = Dimensions.get("screen").width
export const styles = StyleSheet.create({
  buttonStyle: {
    alignItems: "center",
    backgroundColor: color.palette.blue_oec,
    borderRadius: 20,
    height: 50,
    justifyContent: "center",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 25,
    width: 140,
  },
  buttonText: {
    color: color.palette.white,
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  buttonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: screenWidth - 40,
  },
  dateHolder: {
    alignItems: "center",
    borderColor: color.palette.lightGrey,
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: "row",
    fontFamily: "Roboto-Light",
    height: 60,
    justifyContent: "space-between",
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },

  formContainer: {
    alignItems: "center",
    paddingTop: 30,
  },
  formInput: {
    borderColor: color.palette.lightGrey,
    borderRadius: 20,
    borderWidth: 1,
    fontFamily: "Roboto-Light",
    height: 60,
    marginVertical: 5,
    paddingHorizontal: 15,
    width: screenWidth - 60,
  },
  input: {
    color: color.palette.lightGrey,
    flex: 1,
    fontFamily: "Roboto-Light",
    height: 50,
    marginLeft: 5,
    overflow: "hidden",
    paddingHorizontal: 10,
  },
  modalTitle: {
    color: color.palette.blue_oec,
    fontFamily: "Roboto-Light",
    fontSize: 25,
    marginBottom: 20,
    marginTop: 20,
  },
})
