/* eslint-disable react/jsx-no-undef */
/* eslint-disable react-native/no-inline-styles */
import React, { FC, useState, useEffect } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { ScrollView, TextInput, View, TouchableOpacity, Text, Alert } from "react-native"
import { color } from "../../theme"
import { styles } from "./admin-edit-common-holiday.style"
import DateTimePickerModal from "react-native-modal-datetime-picker"
import { faCalendar } from "@fortawesome/free-solid-svg-icons"
import { translate } from "../../i18n"
import RNPickerSelect from "react-native-picker-select"
import { usecommonHolidayStore } from "../../models/common-holiday-store/common-holiday-store"
import { Api } from "../../services/api"
import Toast from "react-native-toast-message"
import { useNavigation } from "@react-navigation/core"

const AdminEditCommonHolidayScreen: FC = (props) => {
  const registerData = props.route.params.data
  type CommonHolidayDataType = {
    name: string
    startDate: string
    endDate: string
    description: string
    type: string
  }
  const [startDate, setStartDate] = useState<Date>(new Date())
  const [endDate, setEndDate] = useState<Date>(new Date())
  const [startDateText, setStartDateText] = useState<string>(registerData.startDate)
  const [endDateText, setEndDateText] = useState<string>(registerData.endDate)
  const [name, setName] = useState<string>(registerData.name)
  const [typeConge, setTypeConge] = useState<string>(registerData.type)
  const [description, setDescription] = useState<string>(registerData.description)
  const [isAddLoading, setIsAddLoading] = useState<boolean>(false)
  const [isDateOpened, setIsDateOpened] = useState<boolean>(false)
  const [isEndDateOpened, setIsEndDateOpened] = useState<boolean>(false)

  const [commonHolidayData, setcommonHolidayData] = useState<CommonHolidayDataType>({
    name: "",
    startDate: "",
    endDate: "",
    description: "",
    type: "",
  })

  useEffect(() => {
    return () => {
      console.log("gone")
    }
  }, [])

  const navigation = useNavigation()
  const commonHolidayStore = usecommonHolidayStore()

  const UpdateButtonAlert = (idHoliday, commonHolidayData) =>
    Alert.alert("Update Common Holiday", translate("Toast.editUser"), [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          handleUpdate(idHoliday, commonHolidayData)
        },
      },
    ])

  const handleUpdate = (idHoliday, commonHolidayData) => {
    setIsAddLoading(true)
    const api = new Api()

    commonHolidayStore.updateCommonHoliday(api, idHoliday, commonHolidayData).then((response) => {
      console.log(response.data)

      if (response.status === 200) {
        Toast.show({
          type: "success",
          text1: "Common Holiday Request",
          text2: response.data,
          topOffset: 60,
        })
        console.log(commonHolidayData)
        setIsAddLoading(false)
        setName("")
        setStartDateText("")
        setEndDateText("")
        setTypeConge("")
        setDescription("")
      } else {
        Toast.show({
          type: "error",
          text1: "Common Holiday Request",
          text2: response.data,
          topOffset: 60,
        })

        setIsAddLoading(false)
        setIsAddLoading(false)
        setName("")
        setStartDateText("")
        setEndDateText("")
        setTypeConge("")
        setDescription("")
      }
    })
  }

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{ alignItems: "center", paddingBottom: 10 }}
    >
      <View style={styles.formContainer}>
        <Text style={styles.modalTitle}>{translate("editCommonHoliday.editHoliday")} </Text>
        <DateTimePickerModal
          mode="date"
          isVisible={isDateOpened}
          onConfirm={(startDate) => {
            setIsDateOpened(false)
            setStartDate(startDate)
            setStartDateText(startDate.toDateString())
          }}
          onCancel={() => {
            setIsDateOpened(false)
          }}
        />

        <DateTimePickerModal
          mode="date"
          isVisible={isEndDateOpened}
          onConfirm={(date) => {
            setIsEndDateOpened(false)
            setEndDate(date)
            setEndDateText(date.toDateString())
            console.log("Date Came From Ending Date")
          }}
          onCancel={() => {
            setIsEndDateOpened(false)
          }}
        />
        <View style={styles.dateHolder}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => {
              setName(text)
            }}
            value={name}
            placeholder={translate("typeConge.holidayName")}
            placeholderTextColor={color.palette.lightGrey}
          />
        </View>
        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateDebut")}
            placeholderTextColor={color.palette.lightGrey}
            editable={false}
            value={startDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        </View>

        <View style={styles.dateHolder}>
          <TextInput
            placeholder={translate("typeConge.dateFin")}
            editable={false}
            value={endDateText}
          />
          <TouchableOpacity
            onPress={() => {
              setIsEndDateOpened(true)
            }}
          >
            <FontAwesomeIcon icon={faCalendar} size={25} color={color.palette.blue_oec} />
          </TouchableOpacity>
        </View>
        <View style={[styles.formInput, { justifyContent: "center" }]}>
          <RNPickerSelect
            value={typeConge}
            style={{
              inputAndroid: { color: "black" },
            }}
            onValueChange={(value) => {
              setTypeConge(value)
            }}
            items={[
              { label: "Paye", value: "paye" },
              { label: "Non paye", value: "nonPaye" },
            ]}
          ></RNPickerSelect>
        </View>

        <View style={styles.dateHolder}>
          <TextInput
            style={styles.input}
            onChangeText={(text) => {
              setDescription(text)
            }}
            value={description}
            placeholder={translate("typeConge.description")}
            placeholderTextColor={color.palette.lightGrey}
          />
        </View>

        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack()
            }}
            style={[styles.buttonStyle, { backgroundColor: color.palette.lightGrey }]}
          >
            <Text style={styles.buttonText}>{translate("common.cancel")}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              console.log("Name :", name)
              setcommonHolidayData({
                name: name,
                startDate: startDateText,
                endDate: endDateText,
                description: description,
                type: typeConge,
              })
              UpdateButtonAlert(registerData.id, {
                name: name,
                startDate: startDateText,
                endDate: endDateText,
                description: description,
                type: typeConge,
              })
            }}
            style={styles.buttonStyle}
          >
            <Text style={styles.buttonText}>{translate("congeScreen.saveButton")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  )
}

export default AdminEditCommonHolidayScreen
